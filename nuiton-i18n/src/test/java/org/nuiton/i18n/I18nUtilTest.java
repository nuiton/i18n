package org.nuiton.i18n;

/*-
 * #%L
 * I18n :: Api
 * %%
 * Copyright (C) 2004 - 2020 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.net.URL;

public class I18nUtilTest {

    @Test
    public void testContainsDirectDirectory() throws Exception {
        URL url = this.getClass().getResource("/zip/not-a-zip.zip");
        Assert.assertFalse(I18nUtil.containsDirectDirectory(url, ""));

        url = this.getClass().getResource("/zip/zip.zip");
        Assert.assertTrue(I18nUtil.containsDirectDirectory(url, "test"));
        Assert.assertFalse(I18nUtil.containsDirectDirectory(url, "testFailing"));
    }
}
