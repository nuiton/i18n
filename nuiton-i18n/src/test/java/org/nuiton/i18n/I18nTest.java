/*
 * #%L
 * I18n :: Api
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.i18n;

import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.i18n.format.MessageFormatI18nMessageFormatter;
import org.nuiton.i18n.init.DefaultI18nInitializer;

import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * Test the class {@link I18n}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.1
 */
public class I18nTest {

    DefaultI18nInitializer initializer;

    @Before
    public void beforeClass() {

        initializer = new DefaultI18nInitializer(
                I18nStoreTest.class.getSimpleName());
    }

    @After
    public void after() {
        I18n.close();
    }

    @Test
    public void testWithNoInit() {

        String expected;
        String actual;

        // pas de traduction possible car l'initialiser n'est pas bon :)

        expected = "key.with.param";
        actual = I18n.t("key.with.param");
        Assert.assertEquals(expected, actual);

    }

    @Test
    public void testWithNoInit2() {

        String expected;
        String actual;

        // pas de traduction possible car l'initialiser n'est pas bon :)

        expected = "key.with.param";
        actual = I18n.l(Locale.FRANCE, "key.with.param");
        Assert.assertEquals(expected, actual);

        actual = I18n.l(Locale.UK, "key.with.param");
        Assert.assertEquals(expected, actual);

    }


    @Test
    public void testDefaultInit() {

        Assert.assertNull(I18n.store);

        I18n.init(null, null);

        Assert.assertNotNull(I18n.store);
        Assert.assertNotNull(I18n.getDefaultLocale());
        Assert.assertEquals(Locale.getDefault(), I18n.getDefaultLocale());
        Assert.assertNotNull(I18n.store.getCurrentLanguage());
        Assert.assertEquals(Locale.getDefault(), I18n.store.getCurrentLocale());

    }

    @Test
    public void testJapanese() {

        I18n.init(initializer, Locale.JAPAN);

        Assert.assertNotNull(I18n.store.resolver.getEncoding());
        Assert.assertEquals("最初の", I18n.t("key.one"));
    }

    @Test
    public void testSimple() {

        String expected;
        String actual;

        // passage en français

        I18n.init(initializer, Locale.FRANCE);

        expected = "Clé avec %s";
        actual = I18n.t("key.with.param");
        Assert.assertEquals(expected, actual);

        expected = "Clé avec param";
        actual = I18n.t("key.with.param", "param");
        Assert.assertEquals(expected, actual);

        // passage en anglais

        I18n.setDefaultLocale(Locale.UK);

        expected = "Key with %s";
        actual = I18n.t("key.with.param");
        Assert.assertEquals(expected, actual);

        expected = "Key with param";
        actual = I18n.t("key.with.param", "param");
        Assert.assertEquals(expected, actual);

        // passage langue inconnue

        I18n.setDefaultLocale(Locale.CHINA);

        expected = "key.with.param";
        actual = I18n.t("key.with.param");
        Assert.assertEquals(expected, actual);

        expected = "key.with.param";
        actual = I18n.t("key.with.param", "param");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testMultiLanguage() {

        String expected;
        String actual;

        // en français

        I18n.init(initializer, null);

        expected = "Clé avec %s";
        actual = I18n.l(Locale.FRANCE, "key.with.param");
        Assert.assertEquals(expected, actual);

        expected = "Clé avec param";
        actual = I18n.l(Locale.FRANCE, "key.with.param", "param");
        Assert.assertEquals(expected, actual);

        // en anglais

        expected = "Key with %s";
        actual = I18n.l(Locale.UK, "key.with.param");
        Assert.assertEquals(expected, actual);

        expected = "Key with param";
        actual = I18n.l(Locale.UK, "key.with.param", "param");
        Assert.assertEquals(expected, actual);

        // dans une langue inconnue

        expected = "key.with.param";
        actual = I18n.l(Locale.CHINA, "key.with.param");
        Assert.assertEquals(expected, actual);

        expected = "key.with.param";
        actual = I18n.l(Locale.CHINA, "key.with.param", "param");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFormatterMessageFormat() {

        initializer.setMessageFormatter(new MessageFormatI18nMessageFormatter());
        I18n.init(initializer, Locale.UK);

        Date date = new GregorianCalendar(2011, 4, 5).getTime();

        // From Java9, the default locale provider is CLDR : https://openjdk.java.net/jeps/252
        // This the expected output depends on the JRE you are using
        String expectedBeforeJava9 = "Key with 05-May-2011";
        String expectedAfterJava8 = "Key with 5 May 2011";
        Set<String> expectedSet = new HashSet<>(Arrays.asList(expectedBeforeJava9, expectedAfterJava8));
        String actual = I18n.l(Locale.UK, "key.with.date", date);
        Assert.assertTrue(actual + " is not one of " + expectedSet, expectedSet.contains(actual));

        String expected = "とキー 2011/05/05";
        actual = I18n.l(Locale.JAPAN, "key.with.date", date);
        Assert.assertEquals(expected, actual);

        expected = "Clé avec 5 mai 2011";
        actual = I18n.l(Locale.FRANCE, "key.with.date", date);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testMissingKeyReturnNull() {

        DefaultI18nInitializer initializer = new DefaultI18nInitializer(
                I18nStoreTest.class.getSimpleName());
        initializer.setMissingKeyReturnNull(false);
        I18n.init(initializer, Locale.FRANCE);

        String key = "youhou";
        String text = I18n.t(key);
        Assert.assertNotNull(text);
        Assert.assertEquals(key, text);

        initializer = new DefaultI18nInitializer(
                I18nStoreTest.class.getSimpleName());
        initializer.setMissingKeyReturnNull(true);
        I18n.init(initializer, Locale.FRANCE);

        text = I18n.t(key);
        Assert.assertNull(text);

        text = I18n.t(key, 123);
        Assert.assertNull(text);
    }

    @Test
    public void testHasKey() {

        I18n.init(initializer, Locale.FRANCE);

        Assert.assertFalse(I18n.hasKey("key.fr.only.missing"));
        Assert.assertFalse(I18n.hasKey("key.fr.only.empty"));
        Assert.assertTrue(I18n.hasKey("key.fr.only.filled"));

        Assert.assertFalse(I18n.hasKey(Locale.FRANCE, "key.fr.only.missing"));
        Assert.assertFalse(I18n.hasKey(Locale.FRANCE, "key.fr.only.empty"));
        Assert.assertTrue(I18n.hasKey(Locale.FRANCE, "key.fr.only.filled"));

        Assert.assertFalse(I18n.hasKey(Locale.JAPAN, "key.fr.only.missing"));
        Assert.assertFalse(I18n.hasKey(Locale.JAPAN, "key.fr.only.empty"));
        Assert.assertFalse(I18n.hasKey(Locale.JAPAN, "key.fr.only.filled"));

    }

}
