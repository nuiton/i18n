/*
 * #%L
 * I18n :: Api
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.bundle;

import org.junit.Assert;
import org.junit.Test;

import java.util.Locale;

/**
 * Tests {@link I18nBundleScope}
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class I18nBundleScopeTest {

    Locale locale;

    I18nBundleScope excepted;

    @Test
    public void testFullScope() {
        excepted = I18nBundleScope.FULL;

        locale = new Locale("fr", "FR");
        Assert.assertEquals(excepted, I18nBundleScope.valueOf(locale));
    }

    @Test
    public void testLanguageScope() {
        excepted = I18nBundleScope.LANGUAGE;

        locale = new Locale("fr");
        Assert.assertEquals(excepted, I18nBundleScope.valueOf(locale));

        locale = new Locale("fr", "");
        Assert.assertEquals(excepted, I18nBundleScope.valueOf(locale));
    }

    @Test
    public void testGeneralScope() {

        excepted = I18nBundleScope.GENERAL;

        locale = null;
        Assert.assertEquals(excepted, I18nBundleScope.valueOf(locale));

        locale = new Locale("");
        Assert.assertEquals(excepted, I18nBundleScope.valueOf(locale));
    }

}
