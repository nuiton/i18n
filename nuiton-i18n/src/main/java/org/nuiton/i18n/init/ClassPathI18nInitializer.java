/*
 * #%L
 * I18n :: Api
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.init;

import io.github.classgraph.ClassGraph;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18nUtil;
import org.nuiton.i18n.bundle.I18nBundle;
import org.nuiton.i18n.bundle.I18nBundleUtil;

import java.net.URL;
import java.util.Iterator;
import java.util.List;


/**
 * Implementation of a {@link I18nInitializer} using all i18n resources (from
 * artifacts) discovered in classpath.
 *
 * Will scan all classpath.
 *
 * This class requires an URLClassLoader, otherwise an exception will be thrown.
 *
 * <b>Note:</b> No order can be predicted with this implementation on bundles.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class ClassPathI18nInitializer extends I18nInitializer {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ClassPathI18nInitializer.class);

    /** class loader to use (optional) */
    protected ClassLoader loader;

    protected URL[] extraURLs;

    public ClassPathI18nInitializer() {
        this(null, null);
    }

    public ClassPathI18nInitializer(ClassLoader loader) {
        this(loader, null);
    }

    public ClassPathI18nInitializer(ClassLoader loader, URL[] extraURLs) {
        this.loader = loader == null ? getClass().getClassLoader() : loader;
        this.extraURLs = extraURLs;
    }

    public URL[] resolvURLs() throws Exception {
        List<URL> urlToSeek = new ClassGraph().getClasspathURLs();

        // on va maintenant supprimer toutes les urls qui ne respectent pas
        // le pattern i18n : il faut que la resource contienne un repertoire i18n
        // ce simple test permet de restreindre la recherche des resources
        // i18n qui est tres couteuse
        int size = urlToSeek.size();
        for (Iterator<URL> it = urlToSeek.iterator(); it.hasNext();) {
            URL url = it.next();
            if (!I18nUtil.containsDirectDirectory(
                    url, I18nBundleUtil.DIRECTORY_SEARCH_BUNDLE_PATTERN)) {
                if (log.isDebugEnabled()) {
                    log.debug("skip url with no " +
                              I18nBundleUtil.DIRECTORY_SEARCH_BUNDLE_PATTERN +
                              " directory : " + url);
                }
                it.remove();
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("detect " + urlToSeek.size() +
                      " i18n capable url (out of " + size + ")");
        }

        // on effectue la recherche des urls des resources i18n (tous les
        // fichiers de traductions) sur toutes les urls precedemment calculees)
        URL[] result = I18nBundleUtil.getURLs(urlToSeek);
        if (log.isDebugEnabled()) {
            for (URL url : result) {
                log.debug(url.toString());
            }
        }
        return result;

    }

    @Override
    public I18nBundle[] resolvBundles() throws Exception {

        // detect bundles urls
        URL[] urls = resolvURLs();

        // detect bundles
        I18nBundle[] result = resolvBundles(urls);
        return result;
    }

    public URL[] getExtraURLs() {
        return extraURLs;
    }

    public ClassLoader getLoader() {
        return loader;
    }

}
