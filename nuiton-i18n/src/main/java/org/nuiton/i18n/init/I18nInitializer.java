/*
 * #%L
 * I18n :: Api
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.init;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.format.I18nMessageFormatter;
import org.nuiton.i18n.I18nUtil;
import org.nuiton.i18n.bundle.I18nBundle;
import org.nuiton.i18n.bundle.I18nBundleEntry;
import org.nuiton.i18n.bundle.I18nBundleUtil;

import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Properties;

/**
 * Abstract object to resolv {@link I18nBundle} and prepare initialization of
 * {@link I18n}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @author Florian Desbois
 * @since 1.1
 */
public abstract class I18nInitializer {

    /** Logger. */
    private static final Log log = LogFactory.getLog(I18nInitializer.class);

    /**
     * Encoding used to load i18n bundles.
     *
     * @since 2.4
     */
    protected Charset encoding;

    /**
     * Message formatter used in {@link I18n} to produce final translated
     * messages.
     *
     * @since 2.4
     */
    protected I18nMessageFormatter messageFormatter;

    /**
     * Used to know if the I18nLanguage has to return null when a key is not
     * found.
     *
     * @since 2.4.1
     */
    protected boolean missingKeyReturnNull;

    protected I18nInitializer() {

        // use the default encoding
        encoding = I18nUtil.DEFAULT_CHARSET;

        // use the default message formatter
        messageFormatter = I18nUtil.DEFAULT_MESSAGE_FORMATTER;

        // by default, missing key returns the key itself
        missingKeyReturnNull = false;
    }

    /**
     * Resolv the bundles.
     *
     * @return the bundles detected
     * @throws Exception if any pb while getting bundles
     */
    public abstract I18nBundle[] resolvBundles() throws Exception;

    public I18nBundle[] resolvBundles(URL... urls) throws Exception {

        // detect bundles

        List<I18nBundle> bundles = I18nBundleUtil.detectBundles(urls);
        I18nBundle[] result = bundles.toArray(new I18nBundle[bundles.size()]);

        if (log.isInfoEnabled()) {

            I18nBundleEntry[] entries = I18nBundleUtil.getBundleEntries(result);

            log.info(bundles.size() + " bundle(s) found, in " +
                     entries.length + " file(s).");
        }
        return result;
    }

    /**
     * Get the {@link Charset} encoding used for i18n {@link Properties} file loading.
     *
     * @return encoding to use
     * @since 2.4
     */
    public Charset getEncoding() {
        return encoding;
    }

    /**
     * Set {@code encoding} to use for i18n {@link Properties} file loading.
     *
     * @param encoding Charset encoding to use
     * @throws NullPointerException if {@code encoding} is null
     * @since 2.4
     */
    public void setEncoding(Charset encoding) throws NullPointerException {
        if (encoding == null) {
            throw new NullPointerException("Can not set a null encoding");
        }
        this.encoding = encoding;
    }

    /**
     * Get the {@link I18nMessageFormatter} to use on each i18n message
     * translation.
     *
     * @return formatter to use
     * @since 2.4
     */
    public I18nMessageFormatter getMessageFormatter() {
        return messageFormatter;
    }

    /**
     * Set {@code messageFormatter} to use on each i18n message translation.
     *
     * @param messageFormatter I18nMessageFormatter to use
     * @throws NullPointerException if {@code messageFormatter} is null
     * @since 2.4
     */
    public void setMessageFormatter(I18nMessageFormatter messageFormatter) throws NullPointerException {
        if (messageFormatter == null) {
            throw new NullPointerException(
                    "Can not set a null message formatter");
        }
        this.messageFormatter = messageFormatter;
    }

    /**
     * Get the {@code missingKeyReturnNull} to use on missing key
     *
     * @return the missingKeyReturnNull parameter
     * @since 2.4.1
     */
    public boolean isMissingKeyReturnNull() {
        return missingKeyReturnNull;
    }

    /**
     * Get the {@code missingKeyReturnNull} to use on missing key
     *
     * @param missingKeyReturnNull missingKeyReturnNull to use
     * @since 2.4.1
     */
    public void setMissingKeyReturnNull(boolean missingKeyReturnNull) {
        this.missingKeyReturnNull = missingKeyReturnNull;
    }

}
