/*
 * #%L
 * I18n :: Api
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.bundle;

import org.nuiton.i18n.I18nUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.Properties;

/**
 * A class to represent an entry in a bundle.
 *
 * The object matches exactly one resource file in a given scope.
 *
 * The object has three properties : <ul> <li> {@link #path} : the path to
 * resource file where to find transaltion for the entry. <li> {@link #locale} :
 * the locale of the entry <li> {link #scope} ; the scope of the entry </ul>
 * This object defines a equals order base on property {@link #path}.
 *
 * This object is {@link Comparable}, the order relation is defined like this :
 * <ul> <li> sort first on {@link #scope}, in the scope order (see {@link
 * I18nBundleScope}), <li> if scopes are equals, sort on {@link #locale} string
 * representation. </ul>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @see I18nBundleScope
 */
public class I18nBundleEntry implements Comparable<I18nBundleEntry> {

    /** path to resource file */
    protected URL path;

    /** local of the entry, can be null if general scope */
    protected Locale locale;

    /** scope of the entry */
    protected I18nBundleScope scope;

    /**
     * Constructor if an bundle entry.
     *
     * It is defined by a <code>path</code> of the resource file, a scope and a
     * locale.
     *
     * @param path   the path of the resource file fo the bundle entry
     * @param locale the given locale of the bundle entry
     * @param scope  the scope of the given entry
     */
    public I18nBundleEntry(URL path, Locale locale, I18nBundleScope scope) {
        this.path = path;
        this.locale = locale;
        this.scope = scope;
    }

    public URL getPath() {
        return path;
    }

    public Locale getLocale() {
        return locale;
    }

    public I18nBundleScope getScope() {
        return scope;
    }

    /**
     * Method to match or not a bundle entry for a given scope and locale.
     *
     * We use the inclusive property of scope, means that we accept all entries
     * on the path to the generalest entry for a givne locale.
     *
     * @param locale the locale to match
     * @param scope  the highest scope to match
     * @return {@code true} if the entry match the scope and locale *
     */
    public boolean matchLocale(Locale locale, I18nBundleScope scope) {
        if (this.locale == null) {
            // a general bundle entry is always matched!
            return true;
        }
        if (locale == null) {
            // can not match a specialized entry with a general scope
            return false;
        }
        // match full locale, or at least a language 
        return this.locale.equals(locale) ||
               this.scope.ordinal() < scope.ordinal() &&
               locale.getLanguage().equals(this.locale.getLanguage());
    }

    /**
     * For a given language, load the resource file of this entry into the
     * <code>resource</code> properties object.
     *
     * @param resource the save of resources already loaded
     * @throws IOException if any pb while reading resource file
     * @deprecated since 2.4 use {@link #load(Properties, Charset)} instead,
     *             charset must be provided to avoid encoding problems
     */
    @Deprecated
    public void load(Properties resource) throws IOException {
        load(resource, I18nUtil.DEFAULT_CHARSET);
    }

    /**
     * For a given language, load the resource file of this entry into the
     * <code>resource</code> properties object. Use {@code charset} to load
     * properties. It could be different from resulting properties store.
     *
     * @param resource the save of resources already loaded
     * @param encoding Charset used to store the properties
     * @throws IOException if any pb while reading resource file
     * @since 2.4
     */
    public void load(Properties resource, Charset encoding) throws IOException {
        InputStream inputStream = null;
        StringBuilder sb = new StringBuilder();
        try {

            inputStream = getPath().openStream();
            if (I18nBundle.log.isDebugEnabled()) {
                sb.append(getPath()).append("\n");
            }

            // Prepare new Properties using charset to load entries
            Properties fileReader = new Properties();
            Reader reader = new InputStreamReader(inputStream, encoding);
            try {
                fileReader.load(reader);
            } finally {
                reader.close();
            }

            if (I18nBundle.log.isDebugEnabled()) {
                for (Entry<Object, Object> entry : fileReader.entrySet()) {
                    sb.append(encoding);
                    sb.append(" : ");
                    sb.append(entry);
                    sb.append("\n");
                }
            }
            for (Entry<Object, Object> entry : fileReader.entrySet()) {
                String key = (String) entry.getKey();
                String value = (String) entry.getValue();
                if (value.trim().isEmpty()) {
                    // if there is a previous sentence loaded but not empty
                    // do not override it
                    String oldValue = (String) resource.get(key);
                    if (oldValue != null) {
                        continue;
                    }
                }
                resource.put(key, value);
            }
            //resource.putAll(fileReader);
            if (I18nBundle.log.isDebugEnabled()) {
                sb.append("nbSentences : ");
                sb.append(fileReader.size());
                sb.append("\n");
                sb.append("=====================================");
            }
            fileReader.clear();

        } finally {
            if (I18nBundle.log.isDebugEnabled()) {
                I18nBundle.log.debug(sb.toString());
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    @Override
    public int compareTo(I18nBundleEntry o) {
        int i = getScope().compareTo(o.getScope());
        if (i == 0) {
            // same scope, sort on locale
            i = getLocale().toString().compareTo(o.getLocale().toString());
        }
        return i;
    }

    @Override
    public boolean equals(Object o) {
        return this == o ||
               o instanceof I18nBundleEntry &&
               path.equals(((I18nBundleEntry) o).path);
    }

    @Override
    public int hashCode() {
        return path.hashCode();
    }

    @Override
    public String toString() {
        String s = super.toString();
        return "<" + s.substring(s.lastIndexOf(".") + 1) + ", locale:" +
               locale + ", scope " + scope + ", path:" + path + ">";
    }
}
