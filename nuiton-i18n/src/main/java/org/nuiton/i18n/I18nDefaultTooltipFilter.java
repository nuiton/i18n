/*
 * #%L
 * I18n :: Api
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.i18n;

/**
 * i18nDefaultTooltipFilter.java
 *
 * Created: 2 déc. 2003
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * @deprecated since 2.4, this filter has nothing to do in i18n api, will be
 *             removed in version 3.0 and never replaced here...
 */
@Deprecated
public class I18nDefaultTooltipFilter implements I18nFilter { // I18nDefaultTooltipFilter

    @Override
    public String applyFilter(String message) {
        if (message != null && message.startsWith("defaultToolTip-")) {
            return null;
        }
        return message;
    }
} // I18nDefaultTooltipFilter

