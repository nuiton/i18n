/*
 * #%L
 * I18n :: Api
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
* BundleBridge.java
*
* Created: 6 sept. 06
*
* @author Arnaud Thimel <thimel@codelutin.com>
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
*/

package org.nuiton.i18n;

import java.util.Enumeration;
import java.util.ResourceBundle;

public class I18nBundleBridge extends ResourceBundle {

    @Override
    public Enumeration<String> getKeys() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object handleGetObject(String key) {
        return I18n.t(key);
    }

}
