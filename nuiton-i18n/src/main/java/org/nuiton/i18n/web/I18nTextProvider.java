/*
 * #%L
 * I18n :: Api
 *
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2018 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.i18n.web;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.DefaultTextProvider;
import com.opensymphony.xwork2.TextProvider;
import com.opensymphony.xwork2.util.ValueStack;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Struts2's TextProvider implementation using nuiton's i18n.
 *
 * To use this text provider add the two following lines in your struts.xml :
 *
 * <pre>
 *     &lt;bean class="org.nuiton.i18n.web.I18nTextProvider" name="i18nTextProvider" type="com.opensymphony.xwork2.TextProvider" /&gt;
 *     &lt;constant name="struts.textProvider" value="i18nTextProvider" /&gt;
 * </pre>
 *
 * Before struts 2.5, you may use "struts.xworkTextProvider" instead of {@link org.apache.struts2.StrutsConstants#STRUTS_TEXT_PROVIDER}
 *
 * @author Arnaud Thimel&lt;thimel@codelutin.com&gt;
 * @since 3.7
 */
public class I18nTextProvider implements TextProvider {

    private static final Log log = LogFactory.getLog(I18nTextProvider.class);

    public static final String UNTRANSLATED_MARKER = "???";

    protected String getSafeText(String key, String value) {
        if (StringUtils.isEmpty(value)) {
            if (log.isWarnEnabled()) {
                log.warn("Key [" + key + "] is not translated");
            }
            return UNTRANSLATED_MARKER + key + UNTRANSLATED_MARKER;
        }
        return value;
    }

    @Override
    public boolean hasKey(String key) {
        Locale locale = getLocale();
        boolean result = I18n.hasKey(locale, key);
        return result;
    }

    /**
     * Implementation copied from {@link DefaultTextProvider#getText(String)}
     */
    protected Locale getLocale() {
        return ActionContext.getContext().getLocale();
    }

    protected String getTextFromLocale(String key, String defaultValue) {
        Locale locale = getLocale();
        String result = I18n.l(locale, key);
        if (result == null) {
            result = defaultValue;
        }
        result = getSafeText(key, result);
        return result;
    }

    protected String getTextFromLocale(String key, String defaultValue, Object ... args) {
        Locale locale = getLocale();
        String result = I18n.l(locale, key, args);
        if (result == null) {
            result = defaultValue;
        }
        result = getSafeText(key, result);
        return result;
    }

    @Override
    public String getText(String aTextName) {
        String value = getTextFromLocale(aTextName, null);
        return value;
    }

    @Override
    public String getText(String aTextName, String defaultValue) {
        String value = getTextFromLocale(aTextName, defaultValue);
        return value;
    }

    @Override
    public String getText(String aTextName,
                          String defaultValue,
                          String obj) {
        String value = getTextFromLocale(aTextName, defaultValue, obj);
        return value;
    }

    @Override
    public String getText(String aTextName, List<?> args) {
        Object[] array = args.toArray();
        String value = getTextFromLocale(aTextName, null, array);
        return value;
    }

    @Override
    public String getText(String key, String[] args) {
        String value = getTextFromLocale(key, null, args);
        return value;
    }

    @Override
    public String getText(String aTextName,
                          String defaultValue,
                          List<?> args) {
        Object[] array = args.toArray();
        String value = getTextFromLocale(aTextName, defaultValue, array);
        return value;
    }

    @Override
    public String getText(String key,
                          String defaultValue,
                          String[] args) {
        String value = getTextFromLocale(key, defaultValue, args);
        return value;
    }

    @Override
    public String getText(String key,
                          String defaultValue,
                          List<?> args,
                          ValueStack stack) {
        String value = getText(key, defaultValue, args);
        return value;
    }

    @Override
    public String getText(String key,
                          String defaultValue,
                          String[] args,
                          ValueStack stack) {
        String value = getText(key, defaultValue, args);
        return value;
    }

    @Override
    public ResourceBundle getTexts(String bundleName) {
        return null;
    }

    @Override
    public ResourceBundle getTexts() {
        return null;
    }

}
