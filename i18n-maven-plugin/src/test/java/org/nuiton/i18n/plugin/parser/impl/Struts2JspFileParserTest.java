package org.nuiton.i18n.plugin.parser.impl;

/*
 * #%L
 * I18n :: Maven Plugin
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.io.SortedProperties;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * To tes the {@link ParserStruts2Mojo.Struts2JspFileParser} parser.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5
 */
public class Struts2JspFileParserTest {

    public static final String ENCODING = "utf-8";

    protected static final SystemStreamLog log = new SystemStreamLog();

    protected ParserStruts2Mojo.Struts2JspFileParser parser;

    protected SortedProperties oldParser;

    protected Set<String> detectedKeys;

    @Before
    public void setUp() throws Exception {
        oldParser = new SortedProperties(ENCODING);


        parser = new ParserStruts2Mojo.Struts2JspFileParser(
                log,
                ENCODING,
                oldParser,
                Pattern.compile("^i18n\\..*$"),
                false) {
            @Override
            protected void registerKey(String key) {
                if (acceptKeyPattern != null) {
                    Matcher matcher = acceptKeyPattern.matcher(key);
                    if (!matcher.matches()) {
                        return;
                    }
                }
                detectedKeys.add(key);
            }
        };
        detectedKeys = new HashSet<String>();
    }

    @After
    public void tearDown() throws Exception {
        detectedKeys.clear();
        detectedKeys = null;
        parser = null;
    }

    @Test
    public void getKeys() throws IOException {

        File file = new File("getKeys");
        String line;

        line = "label='%{getText(\"i18n.key1\")}'/>";
        parseLine(file, line, "i18n.key1");

        line = "key=\"key22\"   label='%{getText(\"i18n.key2\")}'/>   ";
        parseLine(file, line, "i18n.key2");

        line = "<s:text name=\"i18n.key3\"/>";
        parseLine(file, line, "i18n.key3");

        line = "<s:text key='i18n.key4'/>";
        parseLine(file, line, "i18n.key4");

        line = "key=\"key52\"   label=\"%{getText('i18n.key5')}\"/>   ";
        parseLine(file, line, "i18n.key5");

    }

    protected void parseLine(File f, String line, String... expectedKeys) throws IOException {
        parser.parseLine(f, line);
        for (String expectedKey : expectedKeys) {

            Assert.assertTrue("Key " + expectedKey +
                              " was expected from line " + line,
                              detectedKeys.contains(expectedKey));
        }
    }
}
