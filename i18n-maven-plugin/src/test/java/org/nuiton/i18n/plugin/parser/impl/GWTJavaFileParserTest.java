package org.nuiton.i18n.plugin.parser.impl;

/*
 * #%L
 * I18n :: Maven Plugin
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.io.SortedProperties;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;

/**
 * Created on 2/4/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class GWTJavaFileParserTest {

    public static final String ENCODING = "utf-8";

    protected static final SystemStreamLog log = new SystemStreamLog();

    protected ParserGWTJavaMojo.GWTJavaFileParser parser;

    protected SortedProperties oldParser;

    protected Set<String> detectedKeys;

    @Before
    public void setUp() {
        oldParser = new SortedProperties(ENCODING);

        parser = new ParserGWTJavaMojo.GWTJavaFileParser(
                log,
                ENCODING,
                oldParser,
                null,
                false) {
            @Override
            protected void registerKey(String key) {
                detectedKeys.add(key);
            }
        };
        detectedKeys = new HashSet<>();
    }


    @After
    public void tearDown() {
        detectedKeys.clear();
        detectedKeys = null;
        parser = null;
    }

    @Test
    public void testParseFile() throws Exception {
        String classContent = "" +
                              "package org.nuiton.i18n;\n" +
                              "import org.nuiton.i18n.I18n;\n" +
                              "import static org.nuiton.i18n.I18n.t;\n" +
                              "class TestFuctionN {\n" +
                              "@Key(\"a\") String key;\n" +
                              "@   LocalizableResource.Key(\"b\") String key2;\n" +
                              "@com.google.gwt.i18n.client.LocalizableResource.Key(\"c\") String key3;\n" +
                              "@KKey(\"d\") String key4;\n" +
                              "//@KKey(\"e\") String key4;\n" +
                              "/*@KKey(\"f\") String key4;*/\n" +
                              "}";

        File file = new File(FileUtils.getTempDirectory(), "getKeys");
        FileUtils.write(file, classContent, Charset.defaultCharset());

        parseFile(file, new String[]{"d","e","f"}, "a", "b", "c");
    }

    protected void parseFile(File f, String[] notExpectedKeys, String... expectedKeys) throws IOException {
        parser.parseFile(f);

        for (String expectedKey : expectedKeys) {

            Assert.assertTrue("Key " + expectedKey +
                              " was expected from file " + f,
                              detectedKeys.contains(expectedKey));
        }

        for (String expectedKey : notExpectedKeys) {

            Assert.assertFalse("Key " + expectedKey +
                               " was NOT expected from file " + f,
                               detectedKeys.contains(expectedKey));
        }
    }
}
