package org.nuiton.i18n.plugin.parser;

/*
 * #%L
 * I18n :: Maven Plugin
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.i18n.plugin.parser.impl.ParserJavaMojo;
import org.nuiton.i18n.plugin.parser.impl.ParserValidationMojo;

import java.io.File;

/**
 * Tests the {@link SourceEntry}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4.3
 */
public class SourceEntryTest {

    @Test
    public void testGetIncludedFiles() throws Exception {
        SourceEntry entry = new SourceEntry();
        File basedir = new File(".");

        File defaultBasedir = new File(basedir, "src" + File.separator + "test" + File.separator + "java");
        File defaultBasedir2 = new File(basedir, "src" + File.separator + "test" + File.separator + "resources");

        String[] includedFiles;

        includedFiles = entry.getIncludedFiles(defaultBasedir,
                                               new String[]{ParserJavaMojo.DEFAULT_INCLUDES},
                                               I18nSourceEntry.EMPTY_STRING_ARRAY);
        Assert.assertNotNull(includedFiles);
        Assert.assertTrue(includedFiles.length > 0);


        includedFiles = entry.getIncludedFiles(defaultBasedir2,
                                               new String[]{ParserValidationMojo.DEFAULT_INCLUDES},
                                               I18nSourceEntry.EMPTY_STRING_ARRAY);
        Assert.assertNotNull(includedFiles);
        Assert.assertTrue(includedFiles.length > 0);


    }
}
