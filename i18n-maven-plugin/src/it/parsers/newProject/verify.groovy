/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

def checkKeysInFile(path, keys) {

  file = new File(basedir, path);
  if (!file.exists()) {
    println("Could not find file [" + file + "]");
    return false;
  }

  content = file.text;

  for (key in keys) {
    if (!content.contains(key)) {
      println("Could not find " + key + " in file " + file);
      return false;
    }
  }
  return true;
}

assert checkKeysInFile('target/generated-sources/i18n/java.getter',
                       ['javaGetter.key1=', 'javaGetter.key2=', 'javaGetter.key3='])

assert checkKeysInFile('target/generated-sources/i18n/validation.getter',
                       ['validationGetter.key1=', 'validationGetter.key2='])

assert checkKeysInFile('target/generated-sources/i18n/gwt-java.getter',
                       ['gwtJavaGetter.key1=', 'gwtJavaGetter.key2=', 'gwtJavaGetter.key3='])

assert checkKeysInFile('target/generated-sources/i18n/parsers-newProject_fr_FR.properties',
                       [
                       'javaGetter.key1=', 'javaGetter.key2=', 'javaGetter.key3=',
                       'validationGetter.key1=', 'validationGetter.key2=',
                       'gwtJavaGetter.key1=', 'gwtJavaGetter.key2=', 'gwtJavaGetter.key3='
                       ])

assert checkKeysInFile('target/generated-sources/i18n/parsers-newProject_en_GB.properties',
                       [
                       'javaGetter.key1=', 'javaGetter.key2=', 'javaGetter.key3=',
                       'validationGetter.key1=', 'validationGetter.key2=',
                       'gwtJavaGetter.key1=', 'gwtJavaGetter.key2=', 'gwtJavaGetter.key3='
                       ])


return true;
