/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.i18n.test;

import java.util.Locale;

import static org.nuiton.i18n.I18n.t;
import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.l;

public class MyBean {

    protected String field1 = n("javaGetter.key1");

    protected String field2 = t("javaGetter.key2");

    public void method() {
        l(Locale.FRENCH, "javaGetter.key3");
    }
}
