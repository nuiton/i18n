/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.i18n;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.i18n.format.MessageFormatI18nMessageFormatter;
import org.nuiton.i18n.init.DefaultI18nInitializer;
import org.nuiton.i18n.init.I18nInitializer;

import java.util.Locale;

/**
 * Created: 06/05/11
 *
 * @author Florian Desbois
 *         $Id$
 */
public class ReadMessageFormatTest {

    @After
    public void tearDown() {
        I18n.close();
    }

    @Test
    public void testMessageFormatFormatter() {

        I18nInitializer initializer = new DefaultI18nInitializer("ResultBundle");

        initializer.setMessageFormatter(
                new MessageFormatI18nMessageFormatter());

        I18n.init(initializer, Locale.FRANCE);

        String expected = "Message qui fait attention à la conversion de param";
        String actual = I18n.t("lib.property.message2", "param");
        Assert.assertEquals(expected, actual);
    }

}
