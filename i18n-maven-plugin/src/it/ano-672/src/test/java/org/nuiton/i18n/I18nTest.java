/*
 * #%L
 * I18n :: Maven Plugin
 * *
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.i18n;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.i18n.init.DefaultI18nInitializer;

import java.util.Locale;

public class I18nTest {

    public static final String I18n_KEY = "say.hello";

    public static final String FRENCH_HELLO = "Salut";

    public static final String ENGLISH_HELLO = "Hello";

    @Before
    public void setup() {

        I18n.init(new DefaultI18nInitializer("ano-672-i18n"), null);
    }

    @After
    public void after() {
        I18n.close();
    }

    @Test
    public void t() {

        String expected, actual;

        I18n.setDefaultLocale(Locale.FRANCE);

        expected = FRENCH_HELLO;
        actual = I18n.t(I18n_KEY);
        Assert.assertEquals(expected, actual);

        I18n.setDefaultLocale(Locale.ENGLISH);

        expected = ENGLISH_HELLO;
        actual = I18n.t(I18n_KEY);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void l_() {

        String expected, actual;

        expected = FRENCH_HELLO;
        actual = I18n.l(Locale.FRANCE, I18n_KEY);
        Assert.assertEquals(expected, actual);

        expected = ENGLISH_HELLO;
        actual = I18n.l(Locale.UK, I18n_KEY);
        Assert.assertEquals(expected, actual);

    }
}
