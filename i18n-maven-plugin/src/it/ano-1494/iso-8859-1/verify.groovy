/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
def fileExists(path) {

    file = new File(basedir, path);
    return file.exists();

}

def checkContent(path, values) {

  file = new File(basedir, path);

  content = file.text;

  for (value in values) {
    if (!content.contains(value)) {
      println("Could not find " + value + " in file " + file);
      return false;
    }
  }
  return true;
}

assert fileExists('target/classes/org/nuiton/ResultBundle_fr_FR.properties');

assert checkContent('target/generated-sources/i18n/ano-encoding-iso-8859-1_fr_FR.properties',
                    ['Salut \\u00E0 tous']);

assert checkContent('src/main/resources/i18n/ano-encoding-iso-8859-1_fr_FR.properties',
                    ['Salut \\u00E0 tous'])

assert fileExists('target/classes/org/nuiton/ResultBundle_en_GB.properties');

return true;
