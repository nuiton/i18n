package org.nuiton.i18n.plugin.bundle.csv;

/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.nuiton.csv.Import;
import org.nuiton.i18n.plugin.I18nUtil;
import org.nuiton.io.SortedProperties;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Locale;

/**
 * To merge back from a csv bundle to a module i18n bundle files.
 *
 * Created on 7/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.3
 */
@Mojo(name = "merge-back-csv-bundle",
      defaultPhase = LifecyclePhase.GENERATE_RESOURCES,
      requiresProject = true,
      requiresDependencyResolution = ResolutionScope.RUNTIME)
public class MergeBackCsvBundleMojo extends AbstractCsvBundleMojo {

    /**
     * To accept to add new keys in the original i18n bundle.
     *
     * The option is {@code false} by default, since merging operation should not add any new keys.
     */
    @Parameter(property = "i18n.mergeNewKeys", defaultValue = "false")
    protected boolean mergeNewKeys;

    /** Directory where to find project i18n files. */
    @Parameter(property = "i18n.src", defaultValue = "${project.basedir}/src/main/resources/i18n", required = true)
    protected File src;

    /**
     * Name to use as prefix of generated files.
     *
     * <strong>Note :</strong> By default, use the artifact id.
     */
    @Parameter(property = "i18n.artifactId", defaultValue = "${project.artifactId}", readonly = true)
    protected String artifactId;

    /**
     * Location of the csv file to split.
     */
    @Parameter(property = "i18n.bundleCsvFile", required = true)
    protected File bundleCsvFile;

    @Override
    protected void doAction() throws Exception {

        if (!silent) {
            getLog().info("config - locales         : " + Arrays.toString(locales));
            getLog().info("config - csv separator   : " + bundleCsvSeparator);
            getLog().info("config - csv file        : " + bundleCsvFile);
            getLog().info("config - merge directory : " + src);

        }

        // load i18n csv file

        LinkedList<I18nBundleModelRow> rows;

        I18nBundleModel importModel = new I18nBundleModel(bundleCsvSeparator, locales);
        FileInputStream inputStream = FileUtils.openInputStream(bundleCsvFile);
        Import<I18nBundleModelRow> newImport;
        try {
            newImport = Import.newImport(importModel, inputStream);

            rows = Lists.newLinkedList(newImport);
            inputStream.close();
            newImport.close();
        } finally {
            IOUtils.closeQuietly(inputStream);
        }

        // start the merge

        for (Locale locale : locales) {

            File bundleFile = I18nUtil.getI18nFile(src,
                                                   artifactId,
                                                   locale,
                                                   false);

            SortedProperties properties = new SortedProperties(encoding);

            // load old values
            properties.load(bundleFile, encoding);

            // push back values from csv file
            for (I18nBundleModelRow row : rows) {

                String key = row.getKey();

                boolean keyIsPresent = properties.containsKey(key);
                if (mergeNewKeys || keyIsPresent) {

                    properties.put(key, row.getLocaleValue(locale));
                }

            }

            if (!isSilent()) {
                getLog().info(String.format("Merge locale bundle %s in %s", locale, bundleFile));
            }

            properties.store(bundleFile);

        }

    }

}
