/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.nuiton.i18n.plugin.bundle.BundleValidation;
import org.nuiton.io.SortedProperties;

import java.io.File;
import java.util.Arrays;
import java.util.Locale;

/**
 * Merge new generated i18n bundles with older existing ones.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 */
@Mojo(name = "gen", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
@Execute(goal = "get")
public class GenerateMojo extends AbstractI18nGenerateMojo {

    /** Directory where to find project i18n files. */
    @Parameter(property = "i18n.src", defaultValue = "${basedir}/src/main/resources/i18n", required = true)
    protected File src;

    /**
     * To update generated files to user i18n files.
     *
     * <b>Note :</b> By default, this is active, in order to have a project uptodate
     * with last i18n bundles detected.
     */
    @Parameter(property = "i18n.genSrc", defaultValue = "true")
    protected boolean genSrc;

    /**
     * Strict mode to only keep in user i18n detected i18n keys and remove obsolete keys.
     *
     * <b>Note :</b> By default not active. Use this with care since it can
     * delete keys. Moreover if this flag is activated, then all files will be parsed.
     */
    @Parameter(property = "i18n.strictMode", defaultValue = "false")
    protected boolean strictMode;

    /**
     * A flag to check that bundles are complete (no missing i18n translations).
     *
     * @since 1.0.0
     */
    @Parameter(property = "i18n.checkBundle", defaultValue = "true", required = true)
    protected boolean checkBundle;

    /**
     * A flag to show missing i18n translation.
     *
     * <b>Note :</b> Need the {@link #checkBundle} to be activated).
     *
     * @since 1.0.0
     */
    @Parameter(property = "i18n.showEmpty", defaultValue = "false", required = true)
    protected boolean showEmpty;

    @Parameter(property = "i18n.failsIfWarning", defaultValue = "false")
    @Deprecated
    protected boolean failsIfWarning;

    /**
     * A flag to make the build fails if there is some missing key values.
     *
     * <b>Note :</b> This parameter should be used in a release profile to ensure bundles are complete.
     *
     * @since 3.5.1
     */
    @Parameter(property = "i18n.failsIfAnyKeyMissingValue", defaultValue = "false")
    protected boolean failsIfAnyKeyMissingValue;

    /**
     * A flag to make the build fails if there is some missing keys.
     *
     * <b>Note :</b> This parameter should be used in a release profile to ensure bundles are complete.
     *
     * @since 3.5.1
     */
    @Parameter(property = "i18n.failsIfAnyKeyMissingInBundle", defaultValue = "false")
    protected boolean failsIfAnyKeyMissingInBundle;

    /**
     * To keep a backup of old i18n bundles (suffiex by a {@code ~}).
     *
     * <b>Note: </b> By default, this property is not active.
     */
    @Parameter(property = "i18n.keepBackup", defaultValue = "false")
    protected boolean keepBackup;

    /**
     * To keep generated getter files.
     *
     * <b>Note: </b> By default, this property is not active.
     */
    @Parameter(property = "i18n.keepGetters", defaultValue = "false")
    protected boolean keepGetters;
    protected BundleValidation bundleValidation;

    @Override
    protected boolean checkSkip() {
        boolean result = true;
        if (!needGeneration()) {
            getLog().info("No getter detected - all files are up to date.");
            result = false;
        }
        return result;
    }

    @Override
    protected void doAction() throws Exception {
        if (!silent) {
            getLog().info("config - src basedir : " + src.getAbsolutePath());
            getLog().info("config - out basedir : " + out.getAbsolutePath());
            getLog().info("config - locales     : " + Arrays.toString(locales));
        }

        bundleValidation = new BundleValidation(locales);

        for (Locale locale : locales) {
            if (!silent) {
                getLog().info("prepare bundle for locale " + locale);
            }
            // Merge
            File bundleSrc = I18nUtil.getI18nFile(src, artifactId, locale, false);
            File bundleOut = I18nUtil.getI18nFile(out, artifactId, locale, false);
            File bundleGetterOut = I18nUtil.getI18nFile(
                    out, artifactId + GetterMojo.FROM_GETTERS, locale, false);

            SortedProperties propertiesSrc = new SortedProperties(encoding);

            if (bundleSrc.exists()) {
                propertiesSrc.load(bundleSrc);
            }

            SortedProperties propertiesOut = new SortedProperties(encoding);

            if (!strictMode) {
                // si on n'est pas en mode strict, on doit push back in
                // bundle out, all the bundle src keys
                propertiesOut.putAll(propertiesSrc);
            }

            propertiesOut.load(bundleGetterOut);

            // Parcours des clés
            for (Object key : propertiesOut.keySet()) {
                Object oldKey = propertiesOut.get(key);
                Object value = propertiesSrc.get(oldKey);

                // Récupération de la clé si elle a été renommée
                if (!key.equals(oldKey) && value == null) {
                    value = propertiesSrc.get(key);
                }

                if (value != null) {
                    propertiesOut.put(key, value);
                } else {
                    propertiesOut.put(key, "");
                }
            }

            propertiesOut.store(bundleOut);

            // Sauvegarde avant copie
            if (genSrc && keepBackup) {
                backupFile(bundleSrc);
            }
            if (!silent) {
                getLog().info("merge bundle " + locale + " to out");
            }

            if (checkBundle) {

                ImmutableSet<String> keys = Maps.fromProperties(propertiesOut).keySet();
                bundleValidation.getKeysPerLocale().putAll(locale, keys);

                checkBundle(locale, propertiesOut, showEmpty, bundleValidation);

            }

            if (genSrc) {
                // Copie des fichiers dans les sources
//                copyFile(bundleOut, bundleSrc);

                propertiesOut.store(bundleSrc);

                if (!silent) {
                    getLog().info("copy bundle " + locale + " to src");
                }
            }

            if (!keepGetters) {
                if (isVerbose()) {
                    getLog().info("Will delete getter " + bundleGetterOut);
                }
                deleteFile(bundleGetterOut);
            }
        }

        failsIfAnyKeyMissingValue(failsIfWarning || failsIfAnyKeyMissingValue, bundleValidation);
        failsIfAnyKeyMissingInBundle(failsIfAnyKeyMissingInBundle, bundleValidation);

    }
}
