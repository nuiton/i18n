/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin.parser.impl;

import com.opensymphony.xwork2.util.DomHelper;
import org.apache.commons.io.FileUtils;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.nuiton.i18n.plugin.parser.I18nSourceEntry;
import org.nuiton.i18n.plugin.parser.SourceEntry;
import org.nuiton.io.FileUpdater;
import org.nuiton.io.MirroredFileUpdater;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.xpath.XPath;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Find i18n keys from xworks xml validation files.
 *
 * <b>Note: </b> this goal must always be invoked before the {@code process-resources}
 * phase, otherwise all files will be considered as uptodate.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
@Mojo(name = "parserValidation", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class ParserValidationMojo extends AbstractParserXmlMojo {

    public static final String DEFAULT_INCLUDES = "**/*-validation.xml";

    /** Root directory of the default entry. */
    @Parameter(property = "i18n.defaultBasedir", defaultValue = "${basedir}/src/main/resources", required = true)
    protected File defaultBasedir;

    /**
     * Default included files to process (ant-like expression).
     *
     * <strong>Note:</strong> default value is **\/**-validation.xml
     */
    @Parameter(property = "i18n.defaultIncludes", defaultValue = DEFAULT_INCLUDES, required = true)
    protected String defaultIncludes;

    /**
     * Defines the core rules file used to detect i18n keys in xml validation
     * files.
     *
     * <b>Note :</b> If you do not want to use it, set it to empty and fill the
     * {@link #userRulesFiles} parameter.
     *
     * @since 2.0
     */
    @Parameter(property = "i18n.coreRuleFile", defaultValue = "validation.rules", required = true)
    protected String coreRuleFile;

    /**
     * Always use the local xworks dtd to increase performance.
     *
     * @since 1.6.0
     */
    @Parameter(property = "i18n.useLocalResolver", defaultValue = "true")
    protected boolean useLocalResolver;

    /**
     * Defines the file name of the getter where to put detected i18n keys
     * while getter phase.
     *
     * @since 2.0
     */
    @Parameter(property = "i18n.outputGetter", defaultValue = "validation.getter", required = true)
    protected String outputGetter;

    private Map<String, String> dtdMappings;

    public ParserValidationMojo() {

        dtdMappings = new HashMap<String, String>();
        dtdMappings.put("-//Apache Struts//XWork Validator 1.0//EN", "xwork-validator-1.0.dtd");
        dtdMappings.put("-//Apache Struts//XWork Validator 1.0.2//EN", "xwork-validator-1.0.2.dtd");
        dtdMappings.put("-//Apache Struts//XWork Validator 1.0.3//EN", "xwork-validator-1.0.3.dtd");
    }


    @Override
    public String[] getDefaultIncludes() {
        return new String[]{defaultIncludes};
    }

    @Override
    public String[] getDefaultExcludes() {
        return I18nSourceEntry.EMPTY_STRING_ARRAY;
    }

    @Override
    public File getDefaultBasedir() {
        return defaultBasedir;
    }

    @Override
    public FileUpdater newFileUpdater(SourceEntry entry) {
        return new MirroredFileUpdater("", "", entry.getBasedir(), cp) {

            @Override
            public File getMirrorFile(File f) {
                String file =
                        f.getAbsolutePath().substring(prefixSourceDirecotory);
                return new File(destinationDirectory + File.separator + file);
            }
        };
    }

    @Override
    protected String getOutGetter() {
        return outputGetter;
    }

    @Override
    protected String getCoreRuleFile() {
        return coreRuleFile;
    }

    protected XmlFileParser newXmlFileParser(final XPath xpath,
                                             final DocumentBuilder builder) {

        return new XmlFileParser(getLog(),
                                 encoding,
                                 oldParser,
                                 acceptPattern,
                                 showTouchedFiles,
                                 rules,
                                 xpath,
                                 builder,
                                 namespaces,
                                 isVerbose()) {

            @Override
            protected Document fileToDocument(File fileToProcess) throws SAXException, IOException {
                InputSource in = new InputSource(FileUtils.openInputStream(fileToProcess));
                Document doc = DomHelper.parse(in, dtdMappings);
                return doc;
            }

            @Override
            public String extract(String i18nString) {
                String s = null;
                if (!i18nString.trim().isEmpty()) {
                    s = i18nString.trim();
                    int end = s.indexOf("##");
                    if (end > 0) {
                        // remove params from key
                        s = s.substring(0, end);
                    }
                }
                if (getLog().isDebugEnabled()) {
                    getLog().debug(i18nString + " = " + s);
                }
                return s;
            }
        };
    }

}
