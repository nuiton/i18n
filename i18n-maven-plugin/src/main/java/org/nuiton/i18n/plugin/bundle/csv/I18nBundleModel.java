package org.nuiton.i18n.plugin.bundle.csv;

/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.csv.ValueGetterSetter;
import org.nuiton.csv.ext.AbstractImportExportModel;

import java.util.Locale;

/**
 * Created on 7/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.3
 */
public class I18nBundleModel extends AbstractImportExportModel<I18nBundleModelRow> {

    public I18nBundleModel(char separator, Locale[] locales) {
        super(separator);

        newMandatoryColumn("key");
        newColumnForExport("key");

        for (final Locale locale : locales) {

            ValueGetterSetter<I18nBundleModelRow, String> getterSetter = new ValueGetterSetter<I18nBundleModelRow, String>() {
                @Override
                public String get(I18nBundleModelRow row) throws Exception {
                    return row.getLocaleValue(locale);
                }

                @Override
                public void set(I18nBundleModelRow row, String s) throws Exception {
                    row.setLocaleValue(locale, s);
                }
            };
            newColumnForExport(locale.getCountry(), getterSetter);
            newMandatoryColumn(locale.getCountry(), getterSetter);
        }
    }

    @Override
    public I18nBundleModelRow newEmptyInstance() {
        return new I18nBundleModelRow();
    }
}
