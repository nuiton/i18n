/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin.parser;

import org.apache.maven.plugin.logging.Log;
import org.nuiton.io.SortedProperties;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A abstract implementation of a {@link FileParser} with no logic.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public abstract class AbstractFileParser implements FileParser {

    /** Instance logger */
    private final Log log;

    private final SortedProperties oldParser;

    private final boolean showTouchedFiles;

    private final SortedProperties result;

    private boolean touched;

    private final String encoding;

    protected final Pattern acceptKeyPattern;

    protected AbstractFileParser(Log log,
                                 String encoding,
                                 SortedProperties oldParser,
                                 Pattern acceptKeyPattern,
                                 boolean showTouchedFiles) {
        this.log = log;
        this.oldParser = oldParser;
        this.showTouchedFiles = showTouchedFiles;
        this.encoding = encoding;
        result = new SortedProperties(encoding);
        this.acceptKeyPattern = acceptKeyPattern;
    }

    public boolean isShowTouchedFiles() {
        return showTouchedFiles;
    }

    @Override
    public boolean isTouched() {
        return touched;
    }

    @Override
    public SortedProperties getResult() {
        return result;
    }

    public String getEncoding() {
        return encoding;
    }

    @Override
    public void destroy() {
        result.clear();
        touched = false;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        destroy();
    }

    public Log getLog() {
        return log;
    }

    /**
     * Method to invoke when a i18n key was detected .
     *
     * @param key the i18n key to register
     */
    protected void registerKey(String key) {
        if (acceptKeyPattern != null) {
            Matcher matcher = acceptKeyPattern.matcher(key);
            if (!matcher.matches()) {
                return;
            }
        }

        Object value = oldParser.get(key);
        if (value == null) {
            // nouvelle clef du parser, on utilise la clef comme valeur
            value = key;
        }
        // register result
        getResult().put(key, value);

        // one key found in file, so file is marked as touched
        setTouched(true);

        //old code with event
//        String keyModified = key;
//            for (ParserEvent event : events) {
//                event.eventChangeKey(key, !oldLanguage.containsKey(key));
//                keyModified = event.eventGetRealKey();
//            }
//            if (oldParser.containsKey(key)) {
//                result.put(keyModified, oldParser.get(key));
//            } else {
//                result.put(keyModified, key);
//            }
    }

    protected void setTouched(boolean touched) {
        this.touched = touched;
    }

    /**
     * To prepare the file (if any thing to be done before scanning it).
     *
     * By default do nothing, use directly the input file.
     *
     * @param file the incoming file
     * @return the real file to process
     * @throws IOException if any IO problem while preparing file
     * @since 2.1
     */
    protected File prepareFile(File file) throws IOException {
        return file;
    }
}
