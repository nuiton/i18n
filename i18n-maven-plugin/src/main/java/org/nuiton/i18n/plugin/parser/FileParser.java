/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin.parser;

import org.nuiton.io.SortedProperties;

import java.io.File;
import java.io.IOException;


/**
 * the contract of a i18n file parser.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public interface FileParser {

    /**
     * Gets encoding used to read and write files.
     *
     * @return the encoding
     */
    String getEncoding();

    /**
     * @return {@code true} if file was touched (says contains at least one i18n
     *         key)
     */
    boolean isTouched();

    /**
     * TODO As we do not used anylonger old language, we should directly
     *
     * TODO use only a set of keys, no need to keep i18n value...
     *
     * @return the results of i18n keys found for the given file
     */
    SortedProperties getResult();

    /**
     * Parse sur un fichier
     *
     * @param file le fichier à parser
     * @throws IOException if any pb
     */
    void parseFile(File file) throws IOException;

    /**
     * Parse une partie du fichier
     *
     * @param file le fichier à parser
     * @param line la ligne à parser
     * @throws IOException if any pb
     */
    void parseLine(File file, String line) throws IOException;

    /** clean file parser. */
    void destroy();
}
