/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2016 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.i18n.plugin.parser.impl;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.apache.commons.io.IOUtils;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.nuiton.i18n.plugin.parser.AbstractFileParser;
import org.nuiton.i18n.plugin.parser.AbstractI18nParserMojo;
import org.nuiton.i18n.plugin.parser.FileParser;
import org.nuiton.i18n.plugin.parser.I18nSourceEntry;
import org.nuiton.i18n.plugin.parser.ParserException;
import org.nuiton.i18n.plugin.parser.SourceEntry;
import org.nuiton.i18n.plugin.parser.java.Java8BaseVisitor;
import org.nuiton.i18n.plugin.parser.java.Java8Lexer;
import org.nuiton.i18n.plugin.parser.java.Java8Parser;
import org.nuiton.i18n.plugin.parser.java.Java8Parser.SingleElementAnnotationContext;
import org.nuiton.i18n.plugin.parser.java.Java8Parser.TypeNameContext;
import org.nuiton.io.FileUpdater;
import org.nuiton.io.MirroredFileUpdater;
import org.nuiton.io.SortedProperties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * To detect from GWT java files new I18n keys, says content of patterns :
 * <ul>
 * <li>{@code @Key("XXX")}</li>
 * <li>{@code @LocalizableResource.Key("XXX")}</li>
 * <li>{@code @com.google.gwt.i18n.client.LocalizableResource.Key("XXX")}</li>
 * </ul>
 * <b>Note: </b> this goal must always be invoked before the {@code process-resources}
 * phase, otherwise all files will be considered as uptodate.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.1
 */
@Mojo(name = "parserGWTJava", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class ParserGWTJavaMojo extends AbstractI18nParserMojo {

    public static final String DEFAULT_INCLUDES = "**/*.java";

    /** Root directory of the default entry. */
    @Parameter(property = "i18n.defaultBasedir", defaultValue = "${basedir}/src/main/java")
    protected File defaultBasedir;

    /**
     * Default included files to process (ant-like expression).
     *
     * <strong>Note:</strong> default value is **\/*.java
     */
    @Parameter(property = "i18n.defaultIncludes", defaultValue = DEFAULT_INCLUDES, required = true)
    protected String defaultIncludes;

    /**
     * Defines the file name of the getter where to put detected i18n keys
     * while getter phase.
     *
     * @since 2.0
     */
    @Parameter(property = "i18n.outputGetter", defaultValue = "gwt-java.getter")
    protected String outputGetter;

    /**
     * Where to generated temporary processed files.
     *
     * @since 2.0
     */
    @Parameter(property = "i18n.workdir", defaultValue = "${basedir}/target/i18n-workdir")
    protected File workdir;

    protected MirroredFileUpdater entryUpdater;

    @Override
    public String[] getDefaultIncludes() {
        return new String[]{defaultIncludes};
    }

    @Override
    public String[] getDefaultExcludes() {
        return I18nSourceEntry.EMPTY_STRING_ARRAY;
    }

    @Override
    public File getDefaultBasedir() {
        return defaultBasedir;
    }

    @Override
    protected boolean onEnterEntry(I18nSourceEntry entry) {
        boolean b = super.onEnterEntry(entry);
        if (!b) {

            // no skipped entry
            // keep the file updater
            entryUpdater = (MirroredFileUpdater) entry.getUpdater();
        }
        return b;
    }

    @Override
    public FileUpdater newFileUpdater(SourceEntry entry) {
        return new MirroredFileUpdater("", "", entry.getBasedir(), workdir) {

            @Override
            public File getMirrorFile(File f) {
                String file =
                        f.getAbsolutePath().substring(prefixSourceDirecotory);
                return new File(destinationDirectory + File.separator + file);
            }
        };
    }

    @Override
    protected String getOutGetter() {
        return outputGetter;
    }

    @Override
    public FileParser newFileParser(Pattern acceptPattern) {

        return new GWTJavaFileParser(getLog(),
                                     encoding,
                                     oldParser,
                                     acceptPattern,
                                     isShowTouchedFiles()
        );
    }

    protected static class GWTJavaFileParser extends AbstractFileParser {


        public GWTJavaFileParser(Log log,
                                 String encoding,
                                 SortedProperties oldParser,
                                 Pattern acceptKeyPattern,
                                 boolean showTouchedFiles) {
            super(log, encoding, oldParser, acceptKeyPattern, showTouchedFiles);
        }

        @Override
        public void parseFile(File file) throws IOException {

            Reader inputStream = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8);
            try {
                // parse the file
                TokenStream tokenStream = new CommonTokenStream(new Java8Lexer(CharStreams.fromReader(inputStream)));
                Java8Parser parser = new Java8Parser(tokenStream);

                Java8Parser.CompilationUnitContext compilationUnitContext = parser.compilationUnit();
                compilationUnitContext.accept(new JavaParserVisitor(file));
                inputStream.close();
            } catch (Exception e) {
                throw new ParserException(e);
            } finally {
                IOUtils.closeQuietly(inputStream);
            }
        }

        @Override
        public void parseLine(File file, String line) throws IOException {
        }

        protected class JavaParserVisitor extends Java8BaseVisitor<Void> {

            protected final Set<String> annotationPrefix;

            protected final File file;

            private JavaParserVisitor(File file) {
                this.file = file;
                annotationPrefix = new HashSet<>();
                annotationPrefix.add("Key");
                annotationPrefix.add("LocalizableResource.Key");
                annotationPrefix.add("com.google.gwt.i18n.client.LocalizableResource.Key");
            }
            
            @Override
            public Void visitSingleElementAnnotation(SingleElementAnnotationContext ctx) {
                boolean match = false;
                
                TypeNameContext typeName = ctx.typeName();
                String childText = typeName.getText();
                if (annotationPrefix.contains(childText)) {
                    // key is argument 1 of child 2
                    String firstArgs = ctx.elementValue().getText();
                    if (firstArgs.matches("^\"[^\"]+\"$")) {
                        String key = firstArgs.substring(1).substring(0, firstArgs.length() - 2);
                        if (getLog().isDebugEnabled()) {
                            getLog().debug(file.getName() + " detected key = " + key);
                        }
                        ParserGWTJavaMojo.GWTJavaFileParser.this.registerKey(key);
                        match = true;
                    }
                }

                Void aVoid = null;
                if (!match) {
                    // continue visit
                    aVoid = super.visitSingleElementAnnotation(ctx);
                }
                return aVoid;
            }
        }
    }
}