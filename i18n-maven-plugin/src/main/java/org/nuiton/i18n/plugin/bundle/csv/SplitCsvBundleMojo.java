package org.nuiton.i18n.plugin.bundle.csv;

/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.nuiton.csv.Import;
import org.nuiton.i18n.plugin.I18nUtil;
import org.nuiton.io.SortedProperties;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Locale;

/**
 * To split a previously csv i18n file into simple i18n bundles for the locales defined in {@link #locales}.
 *
 * The order of {@link #locales} will be used to extract for each locale i18n bundle (means the order defined
 * in the csv file is not used). Make sure to use exactly the same order.
 *
 * Created on 7/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.3
 */
@Mojo(name = "split-csv-bundle",
      defaultPhase = LifecyclePhase.GENERATE_RESOURCES,
      requiresProject = true,
      requiresDependencyResolution = ResolutionScope.RUNTIME)
public class SplitCsvBundleMojo extends AbstractCsvBundleMojo {

    /**
     * Name of the bundle to generate.
     */
    @Parameter(property = "i18n.bundleOutputName", defaultValue = "${project.artifactId}-i18n", required = true)
    protected String bundleOutputName;

    /**
     * Root directory where to generate aggregated bundles (this directory will
     * be added as resources of the project).
     */
    @Parameter(property = "i18n.bundleOutputDir", defaultValue = "${project.build.directory}", required = true)
    protected File bundleOutputDir;

    /**
     * Name to use as prefix of generated files.
     *
     * <strong>Note :</strong> By default, use the artifact id.
     */
    @Parameter(property = "i18n.artifactId", defaultValue = "${project.artifactId}", readonly = true)
    protected String artifactId;

    /**
     * Location of the csv file to split.
     */
    @Parameter(property = "i18n.bundleCsvFile", required = true)
    protected File bundleCsvFile;

    @Override
    protected void doAction() throws Exception {

        if (!silent) {
            getLog().info("config - locales          : " + Arrays.toString(locales));
            getLog().info("config - bundle directory : " + bundleOutputDir);
            getLog().info("config - bundle name      : " + bundleOutputName);
            getLog().info("config - csv separator    : " + bundleCsvSeparator);
            getLog().info("config - csv file         : " + bundleCsvFile);

        }

        LinkedList<I18nBundleModelRow> rows;

        I18nBundleModel importModel = new I18nBundleModel(bundleCsvSeparator, locales);
        FileInputStream inputStream = FileUtils.openInputStream(bundleCsvFile);
        Import<I18nBundleModelRow> newImport;
        try {
            newImport = Import.newImport(importModel, inputStream);

            rows = Lists.newLinkedList(newImport);
            inputStream.close();
            newImport.close();
        } finally {
            IOUtils.closeQuietly(inputStream);
        }

        for (Locale locale : locales) {

            File bundleFile = I18nUtil.getI18nFile(bundleOutputDir,
                                                   artifactId,
                                                   locale,
                                                   false);

            SortedProperties properties = new SortedProperties(encoding);

            for (I18nBundleModelRow row : rows) {
                properties.put(row.getKey(), row.getLocaleValue(locale));
            }

            if (!isSilent()) {
                getLog().info(String.format("Extract locale bundle %s to %s", locale, bundleFile));
            }
            properties.store(bundleFile);

        }

    }

    @Override
    public void init() throws Exception {
        super.init();

        if (isVerbose()) {
            getLog().info("Will split bundles in " + bundleOutputDir);
        }
        createDirectoryIfNecessary(bundleOutputDir);

    }

}
