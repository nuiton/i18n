package org.nuiton.i18n.plugin;

/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;

/**
 * Created on 7/27/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.3
 */
public abstract class AbstractI18nGenerateMojo extends AbstractI18nMojo {

    /** Le nombre de getters détectés pendant le cycle de vie du build. */
    private static int NB_GETTER_FILES;

    /**
     * Name to use as prefix of generated files.
     *
     * <b>Note :</b> By default, use the artifact id.
     */
    @Parameter(property = "i18n.artifactId", defaultValue = "${project.artifactId}", readonly = true)
    protected String artifactId;

    /** Directory where to generate i18n files. */
    @Parameter(property = "i18n.out", defaultValue = "${basedir}/target/generated-sources/i18n", required = true)
    protected File out;

    /**
     * @return {@code true} si des getters ont etes enregistres pendant le
     * cycle de vie, {@code false} sinon.
     */
    protected boolean needGeneration() {
        boolean needGeneration = NB_GETTER_FILES > 0;
        return needGeneration;
    }

    /**
     * Prend en compte qu'un getter a été détecté.
     *
     * Cela veut dire qu'un goal de parser a détecté des clefs. Il faudra donc
     * activer les goal get et gen.
     */
    protected void addGetter() {
        NB_GETTER_FILES++;
    }

    /**
     * @param root   le repertoire ou sont stockes les fichiers getter
     * @param getter le nom du getter
     * @param create {@code true} pour creer le fichier si non present
     * @return le fichier i18n
     * @throws IOException si probleme lors de la creation du fichier
     */
    protected File getGetterFile(File root, String getter, boolean create)
            throws IOException {
        File file = new File(
                root.getAbsolutePath() + File.separatorChar + getter);
        if (create && !file.exists()) {
            createNewFile(file);
        }
        return file;
    }
}
