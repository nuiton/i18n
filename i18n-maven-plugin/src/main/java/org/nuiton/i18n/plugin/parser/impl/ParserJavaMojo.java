/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2016 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin.parser.impl;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.nuiton.i18n.plugin.parser.AbstractFileParser;
import org.nuiton.i18n.plugin.parser.AbstractI18nParserMojo;
import org.nuiton.i18n.plugin.parser.FileParser;
import org.nuiton.i18n.plugin.parser.I18nSourceEntry;
import org.nuiton.i18n.plugin.parser.ParserException;
import org.nuiton.i18n.plugin.parser.SourceEntry;
import org.nuiton.i18n.plugin.parser.java.Java8BaseVisitor;
import org.nuiton.i18n.plugin.parser.java.Java8Lexer;
import org.nuiton.i18n.plugin.parser.java.Java8Parser;
import org.nuiton.i18n.plugin.parser.java.Java8Parser.ArgumentListContext;
import org.nuiton.i18n.plugin.parser.java.Java8Parser.ExpressionContext;
import org.nuiton.i18n.plugin.parser.java.Java8Parser.MethodInvocationContext;
import org.nuiton.i18n.plugin.parser.java.Java8Parser.MethodInvocation_lfno_primaryContext;
import org.nuiton.i18n.plugin.parser.java.Java8Parser.MethodNameContext;
import org.nuiton.i18n.plugin.parser.java.Java8Parser.TypeNameContext;
import org.nuiton.io.FileUpdater;
import org.nuiton.io.FileUpdaterHelper;
import org.nuiton.io.SortedProperties;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * To parse java files to detect new i18n keys.
 *
 * <b>Note: </b> this goal must always be invoked before the {@code process-resources}
 * phase, otherwise all files will be considered as uptodate.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 */
@Mojo(name = "parserJava", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class ParserJavaMojo extends AbstractI18nParserMojo {

    public static final String DEFAULT_INCLUDES = "**/*.java";

    /** Root directory of the default entry. */
    @Parameter(property = "i18n.defaultBasedir", defaultValue = "${basedir}/src/main/java")
    protected File defaultBasedir;

    /**
     * Default included files to process (ant-like expression).
     *
     * <strong>Note:</strong> default value is **\/*.java
     */
    @Parameter(property = "i18n.defaultIncludes", defaultValue = DEFAULT_INCLUDES, required = true)
    protected String defaultIncludes;

    /**
     * Defines the file name of the getter where to put detected i18n keys
     * while getter phase.
     *
     * @since 2.0
     */
    @Parameter(property = "i18n.outputGetter", defaultValue = "java.getter")
    protected String outputGetter;

    @Override
    public String[] getDefaultIncludes() {
        return new String[]{defaultIncludes};
    }

    @Override
    public String[] getDefaultExcludes() {
        return I18nSourceEntry.EMPTY_STRING_ARRAY;
    }

    @Override
    public File getDefaultBasedir() {
        return defaultBasedir;
    }

    @Override
    public FileUpdater newFileUpdater(SourceEntry entry) {
        return FileUpdaterHelper.newJavaFileUpdater(entry.getBasedir(), cp);
    }

    @Override
    protected String getOutGetter() {
        return outputGetter;
    }

    @Override
    public FileParser newFileParser(Pattern acceptPattern) {

        return new JavaFileParser(getLog(),
                                  encoding,
                                  oldParser,
                                  acceptPattern,
                                  isShowTouchedFiles()
        );
    }

    protected static class JavaFileParser extends AbstractFileParser {

        public JavaFileParser(Log log,
                              String encoding,
                              SortedProperties oldParser,
                              Pattern acceptKeyPattern,
                              boolean showTouchedFiles) {
            super(log, encoding, oldParser, acceptKeyPattern, showTouchedFiles);
        }

        @Override
        public void parseFile(File file) throws IOException {

            // load content
            String content = FileUtils.readFileToString(file, "UTF-8");

            // quick check if there is a org.nuiton.i18n.I18n match in content to avoid a costy parsing
            if (content.contains("org.nuiton.i18n.I18n")) {

                TokenStream tokenStream = new CommonTokenStream(new Java8Lexer(CharStreams.fromString(content)));
                Java8Parser parser = new Java8Parser(tokenStream);

                try {

                    // see http://stackoverflow.com/a/32918434/2038100
                    //parser.setErrorHandler(new BailErrorStrategy());
                    //parser.getInterpreter().setPredictionMode(PredictionMode.SLL);
                    //parser.getInterpreter().tail_call_preserves_sll = false;
                    parser.getInterpreter().enable_global_context_dfa = true;

                    Java8Parser.CompilationUnitContext compilationUnitContext = parser.compilationUnit();
                    compilationUnitContext.accept(new JavaParserVisitor(file));

                } /*catch (ParseCancellationException e) {
                    // see http://stackoverflow.com/a/32918434/2038100
                    parser.setErrorHandler(new DefaultErrorStrategy());
                    parser.getInterpreter().setPredictionMode(PredictionMode.LL);
                    // parser.getInterpreter().tail_call_preserves_sll = false;
                    // parser.getInterpreter().enable_global_context_dfa = true;

                    Java8Parser.CompilationUnitContext compilationUnitContext = parser.compilationUnit();
                    compilationUnitContext.accept(new JavaParserVisitor(file));

                } */catch (Exception e) {
                    throw new ParserException(e);
                }
                
            }
        }

        @Override
        public void parseLine(File file, String line) throws IOException {
        }

        protected class JavaParserVisitor extends Java8BaseVisitor<Void> {

            protected final Set<String> simpleI18nMethodPrefix;

            protected final Set<String> complexI18nMethodPrefix;

            protected final File file;

            private JavaParserVisitor(File file) {
                this.file = file;
                simpleI18nMethodPrefix = new HashSet<String>();
                complexI18nMethodPrefix = new HashSet<String>();
                simpleI18nMethodPrefix.add("org.nuiton.i18n.I18n.n");
                simpleI18nMethodPrefix.add("org.nuiton.i18n.I18n.t");
                simpleI18nMethodPrefix.add("I18n.n");
                simpleI18nMethodPrefix.add("I18n.t");
                simpleI18nMethodPrefix.add("n");
                simpleI18nMethodPrefix.add("t");
                complexI18nMethodPrefix.add("org.nuiton.i18n.I18n.l");
                complexI18nMethodPrefix.add("I18n.l");
                complexI18nMethodPrefix.add("l");
            }
            
            @Override
            public Void visitMethodInvocation_lfno_primary(MethodInvocation_lfno_primaryContext ctx) {

                ArgumentListContext list = ctx.argumentList();
                MethodNameContext mnc = ctx.methodName();
                TerminalNode tn = ctx.Identifier();
                TypeNameContext tnc = ctx.typeName();
                
                Void aVoid = null;
                if (!visitMethod(list, mnc, tn, tnc)) {
                    // continue visit
                    aVoid = super.visitMethodInvocation_lfno_primary(ctx);
                }
                return aVoid;
            }
            
            @Override
            public Void visitMethodInvocation(MethodInvocationContext ctx) {
                ArgumentListContext list = ctx.argumentList();
                MethodNameContext mnc = ctx.methodName();
                TerminalNode tn = ctx.Identifier();
                TypeNameContext tnc = ctx.typeName();

                Void aVoid = null;
                if (!visitMethod(list, mnc, tn, tnc)) {
                    // continue visit
                    aVoid = super.visitMethodInvocation(ctx);
                }
                return aVoid;
            }
            
            protected boolean visitMethod(ArgumentListContext list, MethodNameContext mnc, TerminalNode tn, TypeNameContext tnc) {

                boolean match = false;
                
                if (mnc != null || (tnc != null && tn != null)) {
                    String methodName = mnc != null ? mnc.getText() :
                        tnc.getText() + "." + tn.getText();

                    if (simpleI18nMethodPrefix.contains(methodName)) {
                        // key is argument 1 of child 2
                        ExpressionContext argument = list.expression(0);
                        String firstArgs = argument.getText();
                        if (firstArgs.matches("^\"[^\"]+\"$")) {
                            String key = firstArgs.substring(1).substring(0, firstArgs.length() - 2);
                            if (getLog().isDebugEnabled()) {
                                getLog().debug(file.getName() + " detected key = " + key);
                            }
                            ParserJavaMojo.JavaFileParser.this.registerKey(key);
                            match = true;
                        }
                        
                    } else if (complexI18nMethodPrefix.contains(methodName)) {
                        // key is argument 2 of child 2
                        ExpressionContext argument = list.expression(1);
                        String firstArgs = argument.getText();
                        if (firstArgs.matches("^\"[^\"]+\"$")) {
                            String key = firstArgs.substring(1).substring(0, firstArgs.length() - 2);
                            if (getLog().isDebugEnabled()) {
                                getLog().debug(file.getName() + " detected key = " + key);
                            }
                            ParserJavaMojo.JavaFileParser.this.registerKey(key);
                            match = true;
                        }
                    }
                }

                return match;
            }
        }
    }
}
