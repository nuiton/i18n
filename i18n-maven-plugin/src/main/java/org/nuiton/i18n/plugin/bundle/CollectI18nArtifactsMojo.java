/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin.bundle;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.filter.ArtifactFilter;
import org.apache.maven.artifact.resolver.filter.ScopeArtifactFilter;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.DefaultProjectBuildingRequest;
import org.apache.maven.project.ProjectBuildingRequest;
import org.apache.maven.shared.dependency.graph.DependencyGraphBuilder;
import org.apache.maven.shared.dependency.graph.DependencyGraphBuilderException;
import org.apache.maven.shared.dependency.graph.DependencyNode;
import org.nuiton.i18n.bundle.I18nBundleEntry;
import org.nuiton.plugin.DependencyUtil;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Detects any i18n artifacts in the dependencies of the project and store
 * their references in a file.
 *
 * The generated file will be used by {@code bundle} mojo to generate the final
 * aggregated bundle.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.2
 */
@Mojo(name = "collect-i18n-artifacts",
        defaultPhase = LifecyclePhase.GENERATE_RESOURCES,
        requiresProject = true,
        requiresDependencyResolution = ResolutionScope.RUNTIME)
public class CollectI18nArtifactsMojo extends AbstractI18nBundleMojo {

    /** Directory where to find project i18n files. */
    @Parameter(property = "i18n.src", defaultValue = "${basedir}/src/main/resources/i18n", required = true)
    protected File src;

    /**
     * Local Repository.
     *
     * @since 1.0.2
     */
    @Parameter(property = "localRepository", required = true, readonly = true)
    protected ArtifactRepository localRepository;

    /**
     * Dependency tree builder component.
     *
     * @since 1.0.2
     */
    @Component
    protected DependencyGraphBuilder dependencyTreeBuilder;

    I18nArtifact[] i18nArtifacts;

    @Override
    public void init() throws Exception {
        super.init();

        createDirectoryIfNecessary(collectOutputDir);
    }

    @Override
    protected void doAction() throws Exception {

        // detects the i18n artifacts (only once since it cost some times)...
        i18nArtifacts = detectI18nArtifacts();

        if (i18nArtifacts.length == 0) {
            getLog().warn("no i18n artifact detected.");
            return;
        }

        for (Locale locale : locales) {

            if (!silent) {
                getLog().info("generate collected i18n artifacts for locale " +
                                      locale);
            }
            URL[] urls = getCollectI18nResources(locale);

            if (urls.length == 0) {
                getLog().warn("no i18n bundles for locale " + locale);
                return;
            }

            File bundleOut = getCollectOutputFile(locale, true);

            storeCollectI18nResources(bundleOut, urls);

            getLog().info("collected " + urls.length +
                                  " i18n artifacts for locale " + locale +
                                  " stored in " + bundleOut);
        }
    }

    @Override
    protected URL[] getCollectI18nResources(Locale locale)
            throws IOException {

        // la locale par defaut est la première
        Locale defaultLocale = locales[0];

        List<URL> urls = new ArrayList<URL>();
        for (I18nArtifact artifact : i18nArtifacts) {
            I18nBundleEntry[] bundleEntries =
                    artifact.getBundleEntries(locale, defaultLocale);
            for (I18nBundleEntry bundleEntry : bundleEntries) {

                URL path = bundleEntry.getPath();
                urls.add(path);
                if (verbose) {
                    getLog().info("add " + path);
                }
            }
        }
        return urls.toArray(new URL[urls.size()]);
    }

    protected void storeCollectI18nResources(File bundleOut, URL[] urls)
            throws IOException {
        StringBuilder buffer = new StringBuilder();

        for (URL path : urls) {

            buffer.append(path).append("\n");

            if (verbose) {
                getLog().info("add " + path);
            }
        }
        writeFile(bundleOut, buffer.toString(), encoding);
    }

    /**
     * Detecte les {@link I18nArtifact} et les retourne dans l'ordre de
     * chargement dans le système i18n, i.e l'ordre des dependances entre
     * artifacts.
     *
     * @return les artifacts  i18nables triés par leur ordre de chargement dans
     * le système i18n.
     * @throws IOException while detecting bundles from artifacts
     */
    protected I18nArtifact[] detectI18nArtifacts()
            throws IOException, DependencyGraphBuilderException {

        Map<Artifact, I18nArtifact> dico =
                new HashMap<Artifact, I18nArtifact>();

        I18nArtifact i18nArtifact;
        for (Object o : project.getArtifacts()) {
            i18nArtifact = new I18nArtifact((Artifact) o);
            detectBundles(i18nArtifact, null, dico);
        }

        ArtifactFilter artifactFilter
                = new ScopeArtifactFilter(Artifact.SCOPE_RUNTIME);

        ProjectBuildingRequest buildingRequest = new DefaultProjectBuildingRequest();
        buildingRequest.setLocalRepository(localRepository);
        buildingRequest.setProject(project);
        buildingRequest.setRepositorySession(mavenSession.getRepositorySession());

        DependencyNode rootNode = dependencyTreeBuilder.buildDependencyGraph(
                buildingRequest, artifactFilter);


//        DependencyNode rootNode = dependencyTreeBuilder.buildDependencyGraph(
//                project, localRepository, factory,
//                artifactMetadataSource, artifactFilter, collector);

        List<Artifact> artifacts = new ArrayList<Artifact>(dico.keySet());

        // workaround before using maven-helper-plugin 1.3 see http://nuiton.org/issues/show/1082
        if (!artifacts.isEmpty()) {
            DependencyUtil.sortArtifacts(rootNode, artifacts,
                                         getLog().isDebugEnabled());
        }

        // l'artifact du projet est traite en dernier car s'il possède des
        // bundles alors ils doivent etre charge en dernier

        Artifact projectArtifact = project.getArtifact();


        i18nArtifact = new I18nArtifact(projectArtifact, src.getParentFile());
        detectBundles(i18nArtifact, artifacts, dico);

        I18nArtifact[] result = new I18nArtifact[artifacts.size()];
        int i = 0;
        for (Artifact artifact : artifacts) {
            result[i++] = dico.get(artifact);
        }
        return result;
    }

    protected void detectBundles(I18nArtifact i18nArtifact,
                                 List<Artifact> artifacts,
                                 Map<Artifact, I18nArtifact> dico) throws IOException {
        if (i18nArtifact.detectBundles()) {
            if (!silent) {
                getLog().info("detected i18n artifact " + i18nArtifact);
            }
            if (artifacts != null) {
                artifacts.add(i18nArtifact.getArtifact());
            }
            dico.put(i18nArtifact.getArtifact(), i18nArtifact);
        } else {
            if (getLog().isDebugEnabled()) {
                getLog().debug("reject artifact " + i18nArtifact);
            }
        }
    }
}
