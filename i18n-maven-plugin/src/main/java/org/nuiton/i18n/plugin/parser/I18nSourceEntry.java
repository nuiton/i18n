/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Customized {@link SourceEntry} for parsers goals.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class I18nSourceEntry extends SourceEntry {

    public static final String[] EMPTY_STRING_ARRAY = new String[0];

    public boolean init(AbstractI18nParserMojo mojo) {
        String mojoName = mojo.getClass().getSimpleName();
        if (mojoName.endsWith("Mojo")) {
            mojoName = mojoName.substring(0, mojoName.length() - 4);
        }
        if (!useForGoal(mojoName)) {
            // skip not for this goal
            skipMessage = "exclude for this goal.";
            return true;
        }

        String[] filesForEntry = getFilesForEntry(mojo);

        if (filesForEntry.length == 0) {
            // skip no file found
            skipMessage = "no file found.";
            return true;
        }
        setUpdater(mojo.newFileUpdater(this));

        if (mojo.isStrictMode() || updater == null || mojo.isForce()) {
            // mojo strict/force mode or not updater, so force all files
            skipFiles = EMPTY_STRING_ARRAY;
            files = filesForEntry;
            return false;
        }

        List<String> listFiles = new ArrayList<String>();
        List<String> listSkipFiles = new ArrayList<String>();

        // test if have any file
        for (String foundFile : filesForEntry) {
            File file = new File(getBasedir(), foundFile);
            if (isFileUptodate(file)) {
                listSkipFiles.add(foundFile);
            } else {
                listFiles.add(foundFile);
            }
        }
        boolean todo = !listFiles.isEmpty();
        if (!todo) {
            // skip, no file out-of -date
            skipMessage = "all files are up to date.";
            skipFiles = listSkipFiles.toArray(
                    new String[listSkipFiles.size()]);
            files = EMPTY_STRING_ARRAY;
            return true;
        }
        skipFiles = listSkipFiles.toArray(new String[listSkipFiles.size()]);
        files = listFiles.toArray(new String[listFiles.size()]);
        return false;
    }

    /**
     * Obtain all the relative path of files to treate for a given entry.
     *
     * @param mojo the given mojo
     * @return the list of relative path of files for the given entry
     */
    protected String[] getFilesForEntry(AbstractI18nParserMojo mojo) {
        return getIncludedFiles(mojo.getDefaultBasedir(),
                                mojo.getDefaultIncludes(),
                                mojo.getDefaultExcludes());
    }
}
