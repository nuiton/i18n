/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.nuiton.i18n.I18nUtil;
import org.nuiton.i18n.plugin.bundle.BundleValidation;
import org.nuiton.plugin.AbstractPlugin;
import org.nuiton.plugin.PluginHelper;
import org.nuiton.plugin.PluginWithEncoding;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Lower level i18n mojo abstraction.
 *
 * We defines here all commons parameters and shared behaviour.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @author Julien Ruchaud - ruchaud@codelutin.com
 */
public abstract class AbstractI18nMojo extends AbstractPlugin implements PluginWithEncoding {

    @Parameter( defaultValue = "${session}", readonly = true )
    protected MavenSession mavenSession;

    /**
     * Dependance du projet.
     *
     * @since 1.0.2
     */
    @Parameter(defaultValue = "${project}", readonly = true)
    protected MavenProject project;

    /**
     * Locales to treate, separated by comma.
     *
     * Example :
     * <pre>fr_FR,en_GB</pre>
     */
    @Parameter(property = "i18n.bundles", defaultValue = "fr_FR,en_GB", required = true)
    protected String bundles;

    /** Encoding used to load and store properties. */
    @Parameter(property = "i18n.encoding", defaultValue = "${project.build.sourceEncoding}", required = true)
    protected String encoding;

    /**
     * Verbose flag.
     *
     * <b>Note :</b> if not setted, we used the {@code maven.verbose} property.
     */
    @Parameter(property = "i18n.verbose", defaultValue = "${maven.verbose}")
    protected boolean verbose;

    /**
     * Silent flag to see only errors in console.
     *
     * @since 1.0.0-rc-5
     */
    @Parameter(property = "i18n.silent", defaultValue = "false")
    protected boolean silent;

    /** locales to process */
    protected Locale[] locales;


    @Override
    protected boolean checkPackaging() {

        // nothing to do on a pom module
        boolean result = !acceptPackaging(Packaging.pom);
        return result;
    }

    @Override
    public void init() throws Exception {

        if (verbose) {
            // in verbose mode, no silent
            silent = false;
            getLog().info("config - verbose mode is on");
        }
        locales = I18nUtil.parseLocales(bundles);
        if (locales == null || locales.length == 0) {
            throw new IllegalStateException(
                    "You need at least one locale, please fill the 'bundles' property.");
        }
    }

    protected void checkBundle(Locale locale,
                               Properties propertiesOut,
                               boolean showEmpty,
                               BundleValidation bundleValidation) {

        // on verifie qu'il n'y a pas de traduction vide
        SortedSet<String> emptyEntries = PluginHelper.getEmptyKeys(propertiesOut);
        if (!emptyEntries.isEmpty()) {
            if (bundleValidation != null) {
                Map<Locale, SortedSet<String>> unsafeHolder = bundleValidation.getKeysMissingValues();
                // push empties i18n keys in the holder
                SortedSet<String> empties = unsafeHolder.get(locale);
                if (empties == null) {
                    empties = new TreeSet<String>();
                    unsafeHolder.put(locale, empties);
                }
                empties.addAll(emptyEntries);
            }
            StringBuilder buffer = new StringBuilder();
            int size = emptyEntries.size();
            buffer.append("bundle ");
            buffer.append(locale);
            buffer.append(" contains ");
            buffer.append(size);
            buffer.append("/");
            buffer.append(propertiesOut.size());
            buffer.append(" empty entries!");
            if (showEmpty) {
                int index = 0;
                for (String key : emptyEntries) {
                    buffer.append("\n  - ");
                    buffer.append(index++);
                    buffer.append("/");
                    buffer.append(size);
                    buffer.append(" : ");
                    buffer.append(key);
                }
            } else {
                buffer.append(" (use -Di18n.showEmpty to see these" +
                              " entries)");
            }
            getLog().warn(buffer.toString());
        } else {
            if (!silent && verbose) {
                getLog().info("bundle " + locale + " is valid (no empty" +
                              " entries).");
            }
        }
    }


    @Override
    public File getBackupFile(File file) {
        return new File(file.getAbsolutePath() + "~");
    }

    @Override
    protected void backupFile(File f) throws IOException {
        File dst = getBackupFile(f);
        copyFile(f, dst);
    }

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public boolean isSilent() {
        return silent;
    }

    @Override
    public String getEncoding() {
        return encoding;
    }

    @Override
    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    protected void failsIfAnyKeyMissingValue(boolean failsIfWarning, BundleValidation bundleValidation) throws MojoFailureException {
        if (!failsIfWarning) {

            // no check
            return;
        }

        if (bundleValidation != null && bundleValidation.isAnyKeyMissingValue()) {

            // there is at least one not complete bundle, fails the build
            throw new MojoFailureException(
                    "Bundles for locale(s) " + bundleValidation.getKeysMissingValues().keySet() +
                            " are not complete. Use the -Di18n.showEmpty to see " +
                            "missing translations.");
        }
    }

    protected void failsIfAnyKeyMissingInBundle(boolean failsIfWarning, BundleValidation bundleValidation) throws MojoFailureException {
        if (!failsIfWarning) {

            // no check
            return;
        }

        if (bundleValidation != null && bundleValidation.isAnyKeyMissingInBundle()) {

            // there is at least one not complete bundle, fails the build
            throw new MojoFailureException(
                    "Bundles for locale(s) " + bundleValidation.getMissingKeysPerLocale().keySet() +
                            " are not complete. Use the -Di18n.showEmpty to see " +
                            "missing keys.");
        }
    }
}
