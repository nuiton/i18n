package org.nuiton.i18n.plugin;

/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.plugin.PluginHelper;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

/**
 * Place here some useful methods.
 *
 * Created on 7/27/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.3
 */
public class I18nUtil {
    /**
     * @param root       le repertoire ou sont stockes les fichiers i18n
     * @param artifactId le nom de l'artifact
     * @param locale     le nom de la locale (peut-être nulle)
     * @param create     {@code true} pour creer le fichier si non present
     * @return le fichier i18n
     * @throws IOException si probleme lors de la creation du fichier
     */
    public static File getI18nFile(File root,
                            String artifactId,
                            Locale locale,
                            boolean create) throws IOException {
        String path = root.getAbsolutePath() + File.separatorChar + artifactId;
        if (locale != null) {
            path += "_" + locale.toString();
        }
        path += ".properties";
        File file = new File(path);
        if (create && !file.exists()) {
            PluginHelper.createNewFile(file);
        }
        return file;
    }
}
