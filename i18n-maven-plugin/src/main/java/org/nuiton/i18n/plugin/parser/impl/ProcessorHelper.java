/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.i18n.plugin.parser.impl;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.processor.Processor;
import org.nuiton.processor.ProcessorUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Helper of processor api.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class ProcessorHelper extends ProcessorUtil {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ProcessorHelper.class);

    /**
     * Abstract processor to be used with some smooth logic...
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 2.1
     */
    public abstract static class AbstractParserProcessor extends Processor {

        protected boolean verbose;

        public void setVerbose(boolean verbose) {
            this.verbose = verbose;
        }

        public abstract void process(File filein,
                                     File fileout,
                                     String encoding) throws IOException;

        protected void removefragments(FragmentRemover filter,
                                       File filein,
                                       File fileout,
                                       String encoding,
                                       boolean verbose) throws IOException {

            filter.setVerbose(verbose || log.isDebugEnabled());
            setInputFilter(filter);
            doProcess(this, filein, fileout, encoding);
        }

        public void extractKeys(FragmentExtractor filter,
                                File filein,
                                String encoding,
                                boolean verbose,
                                Set<String> result) throws IOException {

            filter.setVerbose(verbose);
            setInputFilter(filter);

            try (InputStream in = new FileInputStream(filein)) {
                try (OutputStream out = new ByteArrayOutputStream()) {
                    doProcess(this, in, out, encoding);
                    String lines = out.toString();
                    for (String line : lines.split("\n")) {
                        String key = line.trim();
                        if (StringUtils.isNotBlank(key)) {
                            result.add(key);
                        }
                    }
                }
            }
        }

        public void saveKeysToFile(File fileout,
                                   Set<String> keys) throws IOException {
            StringBuilder buffer = new StringBuilder();
            List<String> sortedKeys = new ArrayList<>(keys);
            Collections.sort(sortedKeys);

            for (String key : sortedKeys) {
                buffer.append(key).append("\n");
            }
            FileUtils.writeStringToFile(fileout, buffer.toString(), Charset.defaultCharset());
        }
    }

}
