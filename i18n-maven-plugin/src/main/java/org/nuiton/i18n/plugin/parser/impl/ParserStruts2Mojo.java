package org.nuiton.i18n.plugin.parser.impl;

/*
 * #%L
 * I18n :: Maven Plugin
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.nuiton.i18n.plugin.parser.AbstractFileParser;
import org.nuiton.i18n.plugin.parser.AbstractI18nParserMojo;
import org.nuiton.i18n.plugin.parser.FileParser;
import org.nuiton.i18n.plugin.parser.I18nSourceEntry;
import org.nuiton.i18n.plugin.parser.ParserException;
import org.nuiton.i18n.plugin.parser.SourceEntry;
import org.nuiton.io.FileUpdater;
import org.nuiton.io.MirroredFileUpdater;
import org.nuiton.io.SortedProperties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * To parse struts2 jsp and obtain all keys.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5
 */
@Mojo(name = "parserStruts2", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class ParserStruts2Mojo extends AbstractI18nParserMojo {

    public static final String DEFAULT_INCLUDES = "**/*.jsp";

    /** Root directory of the default entry. */
    @Parameter(property = "i18n.defaultBasedir", defaultValue = "${basedir}/src/main/webapp")
    protected File defaultBasedir;

    /**
     * Default included files to process (ant-like expression).
     *
     * <strong>Note:</strong> default value is **\/*.jsp
     */
    @Parameter(property = "i18n.defaultIncludes", defaultValue = DEFAULT_INCLUDES, required = true)
    protected String defaultIncludes;

    /**
     * Defines the file name of the getter where to put detected i18n keys
     * while getter phase.
     *
     * @since 2.0
     */
    @Parameter(property = "i18n.outputGetter", defaultValue = "struts2.getter")
    protected String outputGetter;

    /** Exploded war Build directory (used to know if files in sources are up-to-date). */
    @Parameter(property = "i18n.explodedWarPath", defaultValue = "${project.build.directory}/${project.artifactId}-{project.version}")
    protected File explodedWarPath;

    @Override
    public String[] getDefaultIncludes() {
        return new String[]{defaultIncludes};
    }

    @Override
    public String[] getDefaultExcludes() {
        return I18nSourceEntry.EMPTY_STRING_ARRAY;
    }

    @Override
    public File getDefaultBasedir() {
        return defaultBasedir;
    }

    @Override
    public FileUpdater newFileUpdater(SourceEntry entry) {
        return new MirroredFileUpdater("", "", entry.getBasedir(), explodedWarPath) {

            @Override
            public File getMirrorFile(File f) {
                String file =
                        f.getAbsolutePath().substring(prefixSourceDirecotory);
                return new File(destinationDirectory + File.separator + file);
            }
        };
    }

    @Override
    protected String getOutGetter() {
        return outputGetter;
    }

    @Override
    public FileParser newFileParser(Pattern acceptPattern) {

        return new Struts2JspFileParser(getLog(),
                                        encoding,
                                        oldParser,
                                        acceptPattern,
                                        isShowTouchedFiles()
        );
    }

    protected static class Struts2JspFileParser extends AbstractFileParser {

        /**
         * Pattern used to detect i18n keys.
         *
         * @since 2.5
         */
        protected final Pattern i18nPattern =
                Pattern.compile("(?:%\\{getText\\(\\\"|text name=\\\"|key=\\\")(.*?)\"|(?:%\\{getText\\('|text\\s*name='|key=')(.*?)'");

        public Struts2JspFileParser(Log log,
                                    String encoding,
                                    SortedProperties oldParser,
                                    Pattern acceptKeyPattern,
                                    boolean showTouchedFiles) {
            super(log, encoding, oldParser, acceptKeyPattern, showTouchedFiles);
        }

        public Pattern getI18nPattern() {
            return i18nPattern;
        }

        @Override
        public void parseFile(File file) throws IOException {
            String line = null;
            LineNumberReader lnr = new LineNumberReader(new InputStreamReader(
                    new FileInputStream(file), getEncoding()));
            try {
                while ((line = lnr.readLine()) != null) {
                    parseLine(file, line);
                }
            } catch (Exception e) {
                if (line != null) {
                    getLog().error(
                            "could not parse line (" + lnr.getLineNumber() + ") '"
                            + line + "' of file " + file);
                }
                throw new ParserException(e);
            } finally {
                lnr.close();
            }
        }

        @Override
        public void parseLine(File file, String line) throws IOException {

            Matcher matcher = i18nPattern.matcher(line);

            while (matcher.find()) {
                String key = matcher.group(1);
                if (key == null) {
                    key = matcher.group(2);
                }
                if (getLog().isDebugEnabled()) {
                    getLog().debug(file.getName() + " detected key = " + key);
                }
                // register key
                registerKey(key);
            }
        }
    }

}