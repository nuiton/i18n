/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2011 CodeLutin, Tony chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.i18n.plugin.bundle;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.component.annotations.Component;

import java.text.MessageFormat;
import java.util.Formatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation of {@link BundleFormatConverter} to convert {@link Formatter}
 * syntax to a {@link MessageFormat} syntax.
 *
 * Created: 05/05/11
 *
 * @author Florian Desbois
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
@Component(role = BundleFormatConverter.class, hint = "toMessageFormat")
public class StringFormatToMessageFormatConverter implements BundleFormatConverter {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(StringFormatToMessageFormatConverter.class);

    protected static Pattern PATTERN = Pattern.compile("%\\$?(\\d?)[^\\s']*");

    @Override
    public String convert(String value) {

        String result;

        Matcher matcher = PATTERN.matcher(value);
        boolean matches = matcher.find();

        if (log.isDebugEnabled()) {
            log.debug("> value : " + value + " _ matches ? " + matches);
        }

        if (matches) {

            // Reset done, because of first find()
            matcher.reset();

            StringBuffer sb = new StringBuffer();
            int i = 0;

            while (matcher.find()) {

                if (log.isDebugEnabled()) {
                    log.debug("> match group : " + matcher.group(0));
                    log.debug("> match group for number : " + matcher.group(1));
                }

                String argNumber = matcher.group(1);

                int nb;

                if (StringUtils.isNotEmpty(argNumber)) {

                    // there is a arg position number, so must use the -1 value
                    nb = Integer.parseInt(argNumber) - 1;
                } else {

                    // use the current argument position value
                    nb = i;
                }

                // Append replacement for current occurence
                matcher.appendReplacement(sb, "\\{" + nb + "\\}");
                i++;
            }
            // Append last chars from input String
            matcher.appendTail(sb);
            result = sb.toString();

            if (log.isDebugEnabled()) {
                log.debug("Result : " + result);
            }

        } else {

            // there is no argument in incoming string value
            result = value;
        }

        // Always escape quote ' to ''
        result = escapeQuoteChar(result);

        return result;
    }

    /**
     * Escape ' char with '', needed by {@link MessageFormat}.
     *
     * @param value Message that contains '
     * @return the message with ' escaped
     */
    protected String escapeQuoteChar(String value) {

        // Replace ' by ''
        String result = value.replaceAll("'", "''");

        if (log.isDebugEnabled()) {
            log.debug("Result with ' escape : " + result);
        }

        return result;
    }

}
