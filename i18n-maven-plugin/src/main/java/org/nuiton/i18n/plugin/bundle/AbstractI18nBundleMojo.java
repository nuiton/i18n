/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin.bundle;

import org.apache.maven.plugins.annotations.Parameter;
import org.nuiton.i18n.plugin.AbstractI18nMojo;
import org.nuiton.i18n.plugin.I18nUtil;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Locale;

/**
 * Created: 26 déc. 2009
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public abstract class AbstractI18nBundleMojo extends AbstractI18nMojo {

    /**
     * Directory where to generate bundles.
     *
     * @since 1.0.2
     */
    @Parameter(property = "i18n.collectOutputDir", defaultValue = "${basedir}/target/i18n", required = true)
    protected File collectOutputDir;

    /**
     * BAse name of file which contains bundles locations.
     *
     * @since 1.0.2
     */
    @Parameter(property = "i18n.collectOutputName", defaultValue = "collect-${project.artifactId}-i18n", required = true)
    protected String collectOutputName;

    /**
     * Pour obtenir les urls des fichiers i18n d'un projet (avec recherche dans
     * ses dépendances).
     *
     * <b>Note:</b> L'ordre des urls reflète l'ordre des dépendances.
     *
     * @param locale la locale a traiter
     * @return les urls des bundles i18n detectees pour le projet.
     * @throws Exception pour tout problème
     */
    protected abstract URL[] getCollectI18nResources(Locale locale)
            throws Exception;

    /**
     * Pour obtenir le fichier contenant les localisation des bundles i18n du
     * projet pour une {@code locale} donnée.
     *
     * @param locale la locale
     * @param create un drapeau pour forcer la création du fichier s'il n'existe
     *               pas
     * @return le fichier qui contient les urls des bundles i18n pour la locale
     *         donnée.
     * @throws IOException pour tout pb
     */
    protected File getCollectOutputFile(Locale locale, boolean create)
            throws IOException {
        File bundleOut = I18nUtil.getI18nFile(collectOutputDir, collectOutputName,
                                              locale, create);
        return bundleOut;
    }

}
