/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin.bundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.maven.artifact.Artifact;
import org.nuiton.i18n.bundle.I18nBundle;
import org.nuiton.i18n.bundle.I18nBundleEntry;
import org.nuiton.i18n.bundle.I18nBundleUtil;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.12
 */
public class I18nArtifact {

    static final Log log = LogFactory.getLog(I18nArtifact.class);

    protected final Artifact artifact;

    protected final URL url;

    protected I18nBundle[] bundles;

    public I18nArtifact(Artifact artifact) throws MalformedURLException {
        this.artifact = artifact;
        url = artifact.getFile().toURI().toURL();
    }

    public I18nArtifact(Artifact artifact,
                        File file) throws MalformedURLException {
        this.artifact = artifact;
        url = file.toURI().toURL();
    }

    public Artifact getArtifact() {
        return artifact;
    }

    public URL getUrl() {
        return url;
    }

    public I18nBundleEntry[] getBundleEntries(Locale l, Locale defaultLocale) {
        if (bundles == null) {
            throw new NullPointerException(
                    "le bundleManager n'a pas ete initialise!");
        }
        return I18nBundleUtil.getBundleEntries(l, defaultLocale, bundles);
    }

    /**
     * Detects the i18n bundles for this artifacts, says in i18n directory.
     *
     * @return {@code true} if artifact has some i18n bunbles, {@code false}
     *         otherwise.
     * @throws IOException if any IO error.
     */
    public boolean detectBundles() throws IOException {

        URL[] i18nUrls = I18nBundleUtil.getURLs(url);

        if (i18nUrls == null || i18nUrls.length == 0) {
            // aucune url sur un fichier de traduction trouve
            // l'artifact n'est pas i18n.
            if (log.isDebugEnabled()) {
                log.debug("no i18n url for artifact " + artifact);
            }
            return false;
        }

        List<I18nBundle> listBundles =
                I18nBundleUtil.detectBundles(i18nUrls);

        if (listBundles.isEmpty()) {
            // pas de bundle instancie (cela ne devrait jamais arrive...)
            return false;
        }

        bundles = listBundles.toArray(new I18nBundle[listBundles.size()]);

        return true;
    }

    @Override
    public String toString() {
        return artifact.getGroupId() + ":" + artifact.getArtifactId() + ":" +
               artifact.getVersion();
    }
}
