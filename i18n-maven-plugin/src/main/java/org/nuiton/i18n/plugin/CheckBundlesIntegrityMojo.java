/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.SortedSetMultimap;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.nuiton.i18n.plugin.bundle.BundleValidation;
import org.nuiton.io.SortedProperties;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;

/**
 * Check bundles integrity. That all keys have a value in all bundles.
 *
 * @since 3.5
 */
@Mojo(name = "check-bundles-integrity", defaultPhase = LifecyclePhase.PREPARE_PACKAGE)
public class CheckBundlesIntegrityMojo extends AbstractI18nGenerateMojo {

    /** Directory where to find project i18n files. */
    @Parameter(property = "i18n.src", defaultValue = "${basedir}/src/main/resources/i18n", required = true)
    protected File src;

    /**
     * A flag to show empty i18n translation.
     *
     * <b>Note :</b> Need the {@link #checkBundle} to be activated).
     *
     * @since 1.0.0
     */
    @Parameter(property = "i18n.showEmpty", defaultValue = "false", required = true)
    protected boolean showEmpty;

    /**
     * A flag to show missing i18n translation.
     *
     * @since 3.5
     */
    @Parameter(property = "i18n.showMissing", defaultValue = "false", required = true)
    protected boolean showMissing;

    /**
     * A flag to make the build fails if there is some warnings while generating
     * bundle, says when it misses some translations.
     *
     * <b>Note :</b> This parameter should be used in a release profile to
     * ensure bundles are complete.
     *
     * @since 2.0
     */
    @Parameter(property = "i18n.failsIfWarning", defaultValue = "false")
    protected boolean failsIfWarning;

    @Override
    protected void doAction() throws Exception {
        if (!silent) {
            getLog().info("config - src basedir : " + src.getAbsolutePath());
            getLog().info("config - locales     : " + Arrays.toString(locales));
        }

        BundleValidation bundleValidation = new BundleValidation(locales);

        for (Locale locale : locales) {

            File bundleSrc = I18nUtil.getI18nFile(src, artifactId, locale, false);

            SortedProperties propertiesSrc = new SortedProperties(encoding);

            if (bundleSrc.exists()) {
                propertiesSrc.load(bundleSrc);
            }

            checkBundle(locale, propertiesSrc, showEmpty, bundleValidation);

            ImmutableSet<String> keys = Maps.fromProperties(propertiesSrc).keySet();
            bundleValidation.getKeysPerLocale().putAll(locale, keys);
        }

        SortedSetMultimap<Locale, String> missingKeysPerLocale = bundleValidation.getMissingKeysPerLocale();
        if (missingKeysPerLocale.isEmpty()) {
            if (!silent) {
                getLog().info("all locales for bundle " + artifactId + " are consistent");
            }
        } else {
            if (showMissing) {
                for (Map.Entry<Locale, String> entry : missingKeysPerLocale.entries()) {
                    Locale locale = entry.getKey();
                    String missingKey = entry.getValue();
                    getLog().warn("bundle " + artifactId + " for locale " + locale + " misses key " + missingKey);
                }
            } else {
                for (Map.Entry<Locale, Collection<String>> entry : missingKeysPerLocale.asMap().entrySet()) {
                    Locale locale = entry.getKey();
                    int numberOfMissingKeys = entry.getValue().size();
                    getLog().warn("bundle " + artifactId + " for locale " + locale + " misses " + numberOfMissingKeys + " keys! (use -Di18n.showMissing to see these entries)");
                }
            }
        }

        if (failsIfWarning && bundleValidation.isFail()) {
            // there is at least one not complete bundle, fail the build
            throw new MojoFailureException("Bundles validation failed, see warning above for details about how to fix");
        }
    }
}
