package org.nuiton.i18n.plugin.bundle.csv;

/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.nuiton.csv.Export;
import org.nuiton.i18n.plugin.I18nUtil;
import org.nuiton.io.SortedProperties;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/**
 * To generate a csv file from the full bundle of the module.
 *
 * The csv file will have a first column with i18n keys, and a
 * column for each locale defined in {@link #locales}.
 *
 * Created on 7/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.3
 */
@Mojo(name = "generate-csv-bundle",
      defaultPhase = LifecyclePhase.GENERATE_RESOURCES,
      requiresProject = true,
      requiresDependencyResolution = ResolutionScope.RUNTIME)
public class GenerateCsvBundleMojo extends AbstractCsvBundleMojo {

    /**
     * Name of the bundle to use.
     */
    @Parameter(property = "i18n.bundleOutputName", defaultValue = "${project.artifactId}-i18n", required = true)
    protected String bundleOutputName;

    /**
     * Encoding used to read any i18n property files.
     *
     * If not defined, will use the {@link #encoding} parameter.
     */
    @Parameter(property = "i18n.bundleInputEncoding")
    protected String bundleInputEncoding;

    /**
     * Encoding used to write any files.
     *
     * If not defined, will use the {@link #encoding} parameter.
     */
    @Parameter(property = "i18n.bundleOutputEncoding")
    protected String bundleOutputEncoding;

    /**
     * Root directory where to generate aggregated bundles (this directory will
     * be added as resources of the project).
     */
    @Parameter(property = "i18n.bundleOutputDir", defaultValue = "${project.build.directory}/generated-sources/resources", required = true)
    protected File bundleOutputDir;

    /**
     * Package name of the generate aggregated bundles.
     *
     * <strong>Note:</strong> By default we use the <code>META-INF</code> package
     * since it is the favorite package of <code>I18n</code> runtime initializer.
     *
     * The package name is dotted as it will be stored as folder like in Java
     * language.
     *
     * Example :
     * <pre>
     *     package name : foo.bar
     *     directory    : foo/bar
     * </pre>
     */
    @Parameter(property = "i18n.bundleOutputPackage", defaultValue = "META-INF", required = true)
    protected String bundleOutputPackage;

    /**
     * To add the generated csv file in the classpath.
     *
     * If {@link #bundleCsvDirectory} is filled, then will add the generated csv file at the root of class-path,
     * otherwise will add add it in the {@link #bundleOutputPackage} package.
     */
    @Parameter(property = "i18n.addInClassPath", defaultValue = "true")
    protected boolean addInClassPath;

    /**
     * Location of the csv file to split.
     */
    @Parameter(property = "i18n.bundleCsvFile", defaultValue = "${i18n.bundleOutputName}-i18n.csv", required = true)
    protected String bundleCsvFileName;

    /**
     * If you want to specify where to generate the csv file.
     *
     * If not filled, then will generate it in the same directory where bundles are stored.
     */
    @Parameter(property = "i18n.bundleCsvDirectory")
    protected File bundleCsvDirectory;

    /**
     * The definitive directory where to load the bundles (includes the package of bundle).
     */
    protected File outputFolder;

    /**
     * The definitive file where to write the csv file.
     */
    protected File bundleCsvFile;

    @Override
    public void init() throws Exception {
        super.init();

        // get the definitive folder where to generate bundles (including bundle package)

        outputFolder = getBundleOutputFolder();

        if (isVerbose()) {
            getLog().info("Will load bundles from " + outputFolder);
        }
        createDirectoryIfNecessary(outputFolder);

        if (StringUtils.isEmpty(bundleInputEncoding)) {

            // use the default encoding
            bundleInputEncoding = getEncoding();
            if (getLog().isDebugEnabled()) {
                getLog().debug("Use as input encoding the default one : " +
                               bundleInputEncoding);
            }
        }

        if (StringUtils.isEmpty(bundleOutputEncoding)) {

            // use the default encoding
            bundleOutputEncoding = getEncoding();

            if (getLog().isDebugEnabled()) {
                getLog().debug("Use as output encoding the default one : " +
                               bundleOutputEncoding);
            }
        }

        if (bundleCsvDirectory == null) {

            if (isVerbose()) {
                getLog().info("Will generate csv bundle in bundle directory " + outputFolder);
            }
            bundleCsvDirectory = outputFolder;

            if (addInClassPath) {

                if (isVerbose()) {
                    getLog().info("Will add " + bundleCsvFileName + " in classpath");
                }
                addResourceDir(bundleOutputDir, "**/" + bundleCsvFileName);
            }
        } else {
            if (isVerbose()) {
                getLog().info("Will generate csv bundle in given directory " + bundleCsvDirectory);
            }

            if (addInClassPath) {

                if (isVerbose()) {
                    getLog().info("Will add " + bundleCsvFileName + " in classpath");
                }
                addResourceDir(bundleCsvDirectory, bundleCsvFileName);
            }
        }
        createDirectoryIfNecessary(outputFolder);

        bundleCsvFile = new File(bundleCsvDirectory, bundleCsvFileName);

        if (!bundleCsvFile.exists()) {
            createNewFile(bundleCsvFile);
        }

    }

    @Override
    protected void doAction() throws Exception {

        if (!isSilent()) {
            getLog().info("config - locales         : " + Arrays.toString(locales));
            getLog().info("config - bundle dir      : " + outputFolder);
            getLog().info("config - bundle name     : " + bundleOutputName);
            getLog().info("config - csv separator   : " + bundleCsvSeparator);
            getLog().info("config - csv file name   : " + bundleCsvFileName);
            getLog().info("config - input encoding  : " + bundleInputEncoding);
            getLog().info("config - output encoding : " + bundleOutputEncoding);
        }

        // fill rows to export

        Map<String, I18nBundleModelRow> rowsByKey = new TreeMap<String, I18nBundleModelRow>();

        for (Locale locale : locales) {

            File bundleFile = I18nUtil.getI18nFile(outputFolder,
                                                   bundleOutputName,
                                                   locale,
                                                   false);

            SortedProperties properties = new SortedProperties(bundleOutputEncoding);
            properties.load(bundleFile);

            for (Object o : properties.keySet()) {
                String key = (String) o;
                I18nBundleModelRow row = rowsByKey.get(key);
                if (row == null) {
                    row = new I18nBundleModelRow();
                    row.setKey(key);
                    rowsByKey.put(key, row);
                }
                row.setLocaleValue(locale, (String) properties.get(o));
            }
        }

        List<I18nBundleModelRow> rows = new LinkedList<I18nBundleModelRow>(rowsByKey.values());
        if (!isSilent()) {
            getLog().info(String.format("Found %d translations to export.", rows.size()));
        }

        // do the export

        if (!isSilent()) {
            getLog().info("Generate csv bundle file at " + bundleCsvFile);
        }

        I18nBundleModel exportModel = new I18nBundleModel(bundleCsvSeparator, locales);

        Export.exportToFile(exportModel, rows, bundleCsvFile, Charset.forName(encoding));

    }

    protected File getBundleOutputFolder() {
        File result = bundleOutputDir;
        if (StringUtils.isNotEmpty(bundleOutputPackage)) {
            String[] paths = bundleOutputPackage.split("\\.");
            for (String path : paths) {
                result = new File(result, path);
            }
        }
        return result;
    }
}
