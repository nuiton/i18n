/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 poussin, Tony Chemit. All rights reserved.
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin.parser;


import org.apache.maven.plugin.logging.Log;
import org.nuiton.io.SortedProperties;
import org.nuiton.plugin.PluginHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author poussin
 * @since 1.2.2
 */
public class ParserExecutor extends ThreadPoolExecutor
        implements I18nParserConfiguration {

    /**
     * the incoming configuration (from mojo which contains shared result and
     * logger)
     */
    protected final I18nParserConfiguration configuration;

    /** list of files consumed */
    protected final List<File> treatedFiles;

    /** list of files touched (says having at least one i18n key) */
    protected final List<File> touchedFiles;

    /** number of files registred to consume */
    protected int nbFiles;

    public ParserExecutor(I18nParserConfiguration configuration) {
        super(8, 10, 1L, TimeUnit.SECONDS, new LinkedBlockingQueue<>());
        this.configuration = configuration;
        touchedFiles = new ArrayList<>();
        treatedFiles = new ArrayList<>();
    }

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        super.afterExecute(r, t);
        ParserTask i18n = (ParserTask) r;
        i18n.registerResult(treatedFiles, touchedFiles, getResult());
    }

    @Override
    public boolean isVerbose() {
        return getConfiguration().isVerbose();
    }

    @Override
    public boolean isSilent() {
        return getConfiguration().isSilent();
    }

    @Override
    public boolean isShowTouchedFiles() {
        return getConfiguration().isShowTouchedFiles();
    }

    @Override
    public Log getLog() {
        return getConfiguration().getLog();
    }

    @Override
    public SortedProperties getResult() {
        return getConfiguration().getResult();
    }

    public List<File> getTreatedFiles() {
        return treatedFiles;
    }

    public List<File> getTouchedFiles() {
        return touchedFiles;
    }

    /** clean internal state after usage of the thread. */
    public void clear() {
        treatedFiles.clear();
        touchedFiles.clear();
        nbFiles = 0;
    }

    /**
     * Add a file to be consumed.
     *
     * @param parser the parser of the file
     * @param files  files to parse
     */
    public void addFile(FileParser parser, File... files) {

        for (File f : files) {

            nbFiles++;
            if (isVerbose()) {
                getLog().info("[" + nbFiles + "] " + f);
            }
            execute(new ParserTask(parser, f));
        }
    }

    /**
     * Ask the thread to stop.
     *
     * It will finish all incoming files (but will not accept more files to
     * parse)
     *
     * <b>Note:</b> The method does not return until all files are not
     * consumed.
     *
     * @throws InterruptedException if something wrong while waiting end of
     *                              executor
     */
    public void terminatesAndWaits() throws InterruptedException {
        // ask executor to terminate
        shutdown();

        if (isVerbose()) {
            if (nbFiles == 0) {
                // no file consumed
                getLog().info("No file consumed.");
            } else {
                getLog().info("Will waits until all files (" + nbFiles +
                              ") are consumed (still " + getNbFilesToTreate() +
                              " file(s) to consume)");
            }
        }

        try {
            // wait until all submited jobs are terminated
            // i don't want timeout, i think 2 days is good :)
            awaitTermination(2, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            getLog().error(e);

            // Restore interrupted state...
            Thread.currentThread().interrupt();
        }

        if (getLog().isDebugEnabled()) {
            getLog().debug("A task was consumed, still " + getNbFilesToTreate() +
                           " file(s) to treate.");
        }

        if (isVerbose()) {
            getLog().info("Executor is terminated.");
        }
    }

    protected I18nParserConfiguration getConfiguration() {
        return configuration;
    }

    protected int getNbFilesToTreate() {
        return nbFiles - treatedFiles.size();
    }

    /**
     * This is a task to parse a {@link #file}.
     *
     * The task will be executed in the executor service created in the thread.
     */
    class ParserTask implements Runnable {

        /** the file parser */
        protected final FileParser parser;

        /** the file to parse */
        protected final File file;

        /** starting time */
        protected long startingTime;

        /** ending time */
        protected long endingTime;

        ParserTask(FileParser parser, File file) {
            this.parser = parser;
            this.file = file;
        }

        @Override
        public void run() {
            startingTime = System.nanoTime();
            if (getLog().isDebugEnabled()) {
                getLog().debug("starting action for " + file);
            }
            try {

                parser.parseFile(file);

            } catch (Exception e) {
                if (getLog().isErrorEnabled()) {
                    getLog().error("could not parse file " + file, e);
                }
            } finally {

                if (getLog().isDebugEnabled()) {
                    getLog().debug("ending action for " + file);
                }
                endingTime = System.nanoTime();
            }
        }

        @Override
        public String toString() {
            return super.toString() + " - " + file;
        }

        protected File getFile() {
            return file;
        }

        protected long getDelay() {
            return endingTime - startingTime;
        }

        protected void destroy() {
            parser.destroy();
        }

        @Override
        protected void finalize() throws Throwable {
            super.finalize();
            destroy();
        }

        /**
         * Register the result of the parsing of the {@link #file} after {@link
         * #run()} method was invoked.
         *
         * This method should be invoked by the executor as an ending hook.
         *
         * @param treatedFiles list of files already treated
         * @param touchedFiles list of files already touched
         * @param result       shared result.
         */
        protected synchronized void registerResult(List<File> treatedFiles,
                                                   List<File> touchedFiles,
                                                   SortedProperties result) {

            try {
                treatedFiles.add(file);
                if (getLog().isDebugEnabled()) {
                    String delay = PluginHelper.convertTime(getDelay());
                    getLog().debug("[" + treatedFiles.size() + "] " + file +
                                   " in " + delay);
                }

                if (parser.isTouched()) {

                    // mark file as touched
                    touchedFiles.add(file);

                    if (isShowTouchedFiles()) {
                        getLog().info("touch " + file);
                    }
                    if (isVerbose()) {
                        String delay = PluginHelper.convertTime(getDelay());
                        getLog().info("[" + treatedFiles.size() + "] touchs " +
                                      file + " in " + delay);
                    }

                    // merge file result with
                    // merge result
                    result.putAll(parser.getResult());
                }
            } finally {

                // destroy runner
                destroy();
            }
        }
    }

}
