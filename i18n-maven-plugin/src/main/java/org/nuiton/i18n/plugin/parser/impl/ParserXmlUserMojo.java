/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.i18n.plugin.parser.impl;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.nuiton.i18n.plugin.parser.I18nSourceEntry;
import org.nuiton.i18n.plugin.parser.SourceEntry;
import org.nuiton.io.FileUpdater;
import org.nuiton.io.MirroredFileUpdater;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.xpath.XPath;
import java.io.File;

/**
 * To obtain i18n keys from xml files using the {@code userRulesFiles} (and
 * an optional {@code coreRuleFile} if any serves your purpose).
 *
 * <b>Note: </b> this goal must always be invoked before the
 * {@code process-resources} phase, otherwise all files will be considered as
 * uptodate.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
@Mojo(name = "parserXml", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class ParserXmlUserMojo extends AbstractParserXmlMojo {

    /**
     * Default directory where to look for files.
     *
     * @since 2.0
     */
    @Parameter(property = "i18n.xmlBasedir", defaultValue = "${basedir}/src/main/resources", required = true)
    protected File basedir;

    /**
     * Defines files to parse.
     *
     * @since 2.0
     */
    @Parameter(property = "i18n.xmlIncludes", required = true)
    protected String includes;

    /**
     * Defines an optional files which contain xpath rules to obtain i18n
     * keys in xml files and is defined in plugin.
     *
     * @since 2.0
     */
    @Parameter(property = "i18n.coreRuleFiles")
    protected String coreRuleFile;

    /**
     * Defines the file name of the getter where to put detected i18n keys while getter phase.
     *
     * @since 2.0
     */
    @Parameter(property = "i18n.outputGetter", defaultValue = "xmlUser.getter", required = true)
    protected String outputGetter;

    @Override
    public String[] getDefaultIncludes() {
        return new String[]{includes};
    }

    @Override
    public String[] getDefaultExcludes() {
        return I18nSourceEntry.EMPTY_STRING_ARRAY;
    }

    @Override
    public File getDefaultBasedir() {
        return basedir;
    }

    @Override
    protected String getCoreRuleFile() {
        return coreRuleFile;
    }

    @Override
    protected XmlFileParser newXmlFileParser(XPath xpath, DocumentBuilder builder) {
        XmlFileParser fileParser = new XmlFileParser(getLog(),
                                                     encoding,
                                                     oldParser,
                                                     acceptPattern,
                                                     showTouchedFiles,
                                                     rules,
                                                     xpath,
                                                     builder,
                                                     namespaces,
                                                     isVerbose()) {
            @Override
            public String extract(String i18nString) {
                String s = null;
                if (!i18nString.trim().isEmpty()) {
                    s = i18nString.trim();
                }
                if (getLog().isDebugEnabled()) {
                    getLog().debug(i18nString + " = " + s);
                }
                return s;
            }
        };
        return fileParser;
    }

    @Override
    protected String getOutGetter() {
        return outputGetter;
    }

    @Override
    public FileUpdater newFileUpdater(SourceEntry entry) {
        return new MirroredFileUpdater("", "", entry.getBasedir(), cp) {

            @Override
            public File getMirrorFile(File f) {
                String file =
                        f.getAbsolutePath().substring(prefixSourceDirecotory);
                return new File(destinationDirectory + File.separator + file);
            }
        };
    }
}