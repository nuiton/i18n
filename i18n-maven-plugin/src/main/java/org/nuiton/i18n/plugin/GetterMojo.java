/*
 * #%L
 * I18n :: Maven Plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.util.DirectoryScanner;
import org.nuiton.io.SortedProperties;
import org.nuiton.plugin.PluginHelper;

import java.io.File;
import java.util.Arrays;
import java.util.Locale;

/**
 * Recupere les différents fichiers des parsers en un fichier de proprietes.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 */
@Mojo(name = "get", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class GetterMojo extends AbstractI18nGenerateMojo {

    /**
     * To keep generated getter files.
     *
     * <b>Note: </b> By default, this property is not active.
     */
    @Parameter(property = "i18n.keepGetters", defaultValue = "false")
    protected boolean keepGetters;

    protected static final String FROM_GETTERS = "-fromGetters";

    @Override
    protected boolean checkSkip() {

        boolean result = true;

        if (!needGeneration()) {
            getLog().info("No getter detected - all files are up to date.");
            result = false;
        }
        return result;
    }

    @Override
    protected void doAction() throws Exception {

        if (!silent) {
            getLog().info("config - basedir : " + out.getAbsolutePath());
            getLog().info("config - locales : " + Arrays.toString(locales));
        }

        File bundleGetters = new File(out.getAbsoluteFile(),
                                      artifactId + ".properties");

        createDirectoryIfNecessary(bundleGetters.getParentFile());

        SortedProperties propertiesOut = new SortedProperties(encoding);

        DirectoryScanner ds = new DirectoryScanner();
        ds.setBasedir(out);
        ds.setIncludes(new String[]{"*.getter"});
        ds.scan();
        String[] files = ds.getIncludedFiles();

        // Fusion des fichiers propriétés des différents parsers
        for (String file : files) {
            long t0 = System.nanoTime();
            File bundleGetter = getGetterFile(out, file, false);

            // chargement du getter
            SortedProperties propertiesIn =
                    new SortedProperties(encoding).load(bundleGetter);

            // ajout des entrées dans le bundle
            propertiesOut.putAll(propertiesIn);

            if (!keepGetters) {
                if (isVerbose()) {
                    getLog().info("Will delete getter " + bundleGetter);
                }
                deleteFile(bundleGetter);
            }
            if (!silent) {
                String time = PluginHelper.convertTime(System.nanoTime() - t0);
                getLog().info("import getter " + bundleGetter.getName() +
                              " in " + time);
            }
        }

        // sauvegarde du fichier des getters
        propertiesOut.store(bundleGetters);

        // Création des bundles
        for (Locale locale : locales) {
            if (getLog().isDebugEnabled()) {
                getLog().debug("generate bundle for locale " + locale);
            }
            File bundleOut = I18nUtil.getI18nFile(out, artifactId + FROM_GETTERS, locale, false);
            copyFile(bundleGetters, bundleOut);
            if (!silent && verbose) {
                getLog().info("generate bundle " + locale);
            }
        }
        if (!keepGetters) {
            deleteFile(bundleGetters);
        }
    }

}
