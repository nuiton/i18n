# Nuiton I18n

Le projet Nuiton I18n fournit des classes Java pour l'internationalisation (i18n) des applications.

Cette librairie très légère combine les très utilisées ResourceBundles Java, une API simple d'utilisation et très puissante ainsi qu'un procédé d'extension très utile. De nombreux autre avantages existent à utiliser Nuiton I18n :

- Extraction des clés de traduction

- Integration au process de build au travers de Maven ou Ant

Petit exemple qui démontre la simplicité d'utilisation de Nuiton I18n :

    I18n.init(new DefaultI18nInitializer("myBundle"), Locale.FRANCE);
    System.out.println(I18n._("This text will be translated"));

Les deux modules du projet Nuiton I18n :

* I18n Api

* I18n Maven Plugin